![Vortoj emblemo](Vortoj_emblemo.png)

# Bonvenon!

**Vortoj**, antaŭe konata subnome de Paŝklaku!, estas ludo kiu helpas al oni certiĝi pri sia vortprovizo de disponeblaj lingvoj.
La ludo utiligas la uzon de danctapiŝoj kaj eĉ rekomendas [tiujn ĉi](https://www.ddr.cz/ddr-cz) pro la kvalito kaj bona kunfunkciado de la ludo kaj enigilo.

# Ecoj de la ludo
* 8 lingovj
* Miloj da vortoj
* Vortaro redakteblas
* Danctapiŝoj agordeblas
* Multaj kategorioj de vortoj kun la ebleco krei sian propran

# Kontribui
Ĉiuj estas bonvenaj kontribui, se vi pretas helpi, se vi kapablas, bonvolu fari unu el la venontaj

## Plibonigi vortprovizon de lingvoj
[Repositorio vorta](https://gitlab.com/lentsius/vortoj_vortoj)

## Kontribui al la sonaro kaj muzikaro
[Muzikara repositorio](https://gitlab.com/lentsius/vortoj_muziko)

## Helpi kun tradukoj
[Ĉeftradukdosiero](https://drive.google.com/open?id=1ideiPJOJNTaQ1JBl9RP0VUa4YlgrZMTnhWZZV9seoSQ)

## Programi novajn funkciojn aŭ helpi plibonigi ekzistantajn
> Elŝutu la projekton kaj malfermu per [Godot 3.1](https://godotengine.org)

Kiam vi pretas kontribui, faru **kuniĝpeton** kaj se ĉio enordas mi aldonos.

Kaze de necerteco de via flanko pri io ajn, hezitu kontakti min kaj siletu eterne.