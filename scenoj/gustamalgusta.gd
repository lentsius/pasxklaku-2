extends Node2D

var muzikpozicio = 0.0

func _ready():
	pass

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().paused = true
		$pauzo.gxisdatigxi()
		$pauzo.set_process(true)
		$pauzo.visible = true
		sxalti_muzikon()
		
	if Input.is_action_pressed("cheat"):
		Engine.time_scale = 20.0
	else:
		Engine.time_scale = 1.0
		
				
func sxalti_muzikon():
	var kanto = $sonoj/muziko.nunakanto
	if kanto.is_playing():
		muzikpozicio = kanto.get_playback_position()
		kanto.stop()
	else:
		kanto.play(muzikpozicio)
