extends VBoxContainer

var lingva_dosierujo = ""
var origin_lingva_dosierujo = ""

var kategorioj : Array
var elektita_kategorio : int = 0

var alia_lingvo : String = ""

func _ready():
	gxisdatigi_lingvojn()


func update(restarta : bool = false):
	if alia_lingvo != "":
		lingva_dosierujo = globala.lingva_dosierujo + alia_lingvo + "/"
		origin_lingva_dosierujo = "res://vortoj/" + globala.lingvo_vorto + "/"
	else:
		lingva_dosierujo = globala.lingva_dosierujo + globala.lingvo_vorto + "/"
		origin_lingva_dosierujo = "res://vortoj/" + globala.lingvo_vorto + "/"
		
	if restarta:
		$lingvo/lingvo.select(globala.lingvo)
		
	var gxustaj : TextEdit = $HBoxContainer2/gxustaj_vortoj_agordo/gxustaj_edit
	gxustaj.text = preni_gxustajn_vortojn()
	gxustaj.cursor_set_line(6)
	
	var malgxustaj : TextEdit = $HBoxContainer2/malgxustaj_vortoj_agordo/malgxustaj_edit
	malgxustaj.text = preni_malgxustajn_vortojn()
	

func gxisdatigi_lingvojn() -> void:
	$lingvo/lingvo.clear()
	for i in globala.lingvoj:
		$lingvo/lingvo.add_item(i)
		
	$lingvo/lingvo.select(globala.lingvo)
	

func gxisdatigi_kategoriojn(language_id : int = 0):
#func gxisdatigi_kategoriojn(language_id : int = 0, alia_lingvo : bool = false):
	$kategorioj/kategorio.clear()
	kategorioj.clear()
	kategorioj = globala.preni_kategoriojn(alia_lingvo)
	
	TranslationServer.set_locale(alia_lingvo)
	
	for i in kategorioj:
		$kategorioj/kategorio.add_item(TranslationServer.translate(i.to_upper()).capitalize())
		
	$kategorioj/kategorio.emit_signal("item_selected", 0)
		
		
func preni_gxustajn_vortojn():
	var dosiero = File.new()	
	dosiero.open(lingva_dosierujo + globala.kategorio + "/gxustaj.txt", File.READ)
	var prenvortoj = dosiero.get_as_text()
	dosiero.close()
	return prenvortoj
	
	
func preni_malgxustajn_vortojn():
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/malgxustaj.txt", File.READ)
	var prenvortoj = dosiero.get_as_text()
	dosiero.close()
	return prenvortoj


func konservi_gxustajn_vortojn(nova_teksto):
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/gxustaj.txt", File.WRITE)
	dosiero.store_string(nova_teksto)
	dosiero.close()


func konservi_malgxustajn_vortojn(nova_teksto):
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/malgxustaj.txt", File.WRITE)
	dosiero.store_string(nova_teksto)
	dosiero.close()
	
	
func konservi_volapukajxojn(nova_teksto):
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/volapukajxoj.txt", File.WRITE)
	dosiero.store_string(nova_teksto)
	dosiero.close()
	
	
# tiu cxi funkcio restartas cxiun vortojn de la nuna lingvo kaj kategorio
func restarti_vortojn():
	restarti_kategorion("/gxustaj.txt")
	restarti_kategorion("/malgxustaj.txt")
	update()


func restarti_kategorion(speco):
	var leg_dosiero = File.new()
	var eraro = leg_dosiero.open(origin_lingva_dosierujo + globala.kategorio + speco, File.READ)
	if eraro:
		print("La kategorio ne estas origina, vi umu.")
		return

	var origin_vortoj = leg_dosiero.get_as_text()
	leg_dosiero.close()

	var skrib_dosiero = File.new()
	
	skrib_dosiero.open(lingva_dosierujo + globala.kategorio + speco, File.WRITE)
	skrib_dosiero.store_string(origin_vortoj)
	skrib_dosiero.close()


func _on_kategorio_item_selected(ID):
	#unue ĝisdatigi la kategoriojn globale kaj ĉi tie
	globala.kategorio = kategorioj[ID]
	elektita_kategorio = ID

	#kontroli ĉu estas protektita
	var videbla : bool = true
	if globala.kontroli_sxlositajn(globala.kategorio):
		videbla = false
	
	#se jes ni kaŝu la butonon por forigi
	$kategorioj/Forigi.visible = videbla
	
	#fine ni ĝisdatigu la listojn kaj tekstojn
	update()


func _on_sensensaj_konservi_pressed():
	konservi_volapukajxojn($HBoxContainer2/eraraj_vortoj_agordo/sensensaj_edit.text)


func _on_konservi_gxustajn_pressed():
	konservi_gxustajn_vortojn($HBoxContainer2/gxustaj_vortoj_agordo/gxustaj_edit.text)


func _on_konservi_malgxustajn_pressed():
	konservi_malgxustajn_vortojn($HBoxContainer2/malgxustaj_vortoj_agordo/malgxustaj_edit.text)


func _on_nuligi2_pressed():
	restarti_vortojn()


func _on_KreiKategorion_kreis_novan_kategorion(nomo : String) -> void:
	gxisdatigi_kategoriojn()
	update()
	print("kreis kategorion ", nomo, " sukcese")
	#var nombro = kategorioj.find(nomo)
	#$kategorioj/kategorio.emit_signal("item_selected", nombro)


func _on_Forigi_pressed():
	var kat = $kategorioj/kategorio.get_item_text(elektita_kategorio)
	print(kat)
	print(globala.sxlositaj_kategorioj)
	if globala.kontroli_sxlositajn(kat):
		print("oni ne rajtas forigi tiun cxi")
		$Tvino.interpolate_property($kategorioj/Protekti, 'modulate:a', 1, 0, 1.5, Tween.TRANS_QUAD, Tween.EASE_OUT, 0.0)
		$Tvino.start()
		return
		
	if alia_lingvo != "":
		$"../ForigiKategorion".lingvo = alia_lingvo
	else:
		$"../ForigiKategorion".lingvo = globala.lingvo_vorto
		
	$"../ForigiKategorion".forigi($kategorioj/kategorio.get_item_text(elektita_kategorio))
	


func _on_ForigiKategorion_forigis_kategorion():
	gxisdatigi_kategoriojn()
	update()


func _on_lingvo_item_selected(ID):
#	print($lingvo/lingvo.get_item_text(ID))
	alia_lingvo = $lingvo/lingvo.get_item_text(ID)
	globala.redaktata_lingvo = alia_lingvo
	gxisdatigi_kategoriojn()
	update()
	


func _on_gxustaj_edit_cursor_changed():
	var nova_linio : int = $HBoxContainer2/gxustaj_vortoj_agordo/gxustaj_edit.cursor_get_line()
	$HBoxContainer2/malgxustaj_vortoj_agordo/malgxustaj_edit.cursor_set_line(nova_linio)


func _on_DosieroButono_pressed():
	OS.shell_open(OS.get_user_data_dir() + "/lingvoj/")


func _on_Konservi_pressed() -> void:
	konservi_gxustajn_vortojn($HBoxContainer2/gxustaj_vortoj_agordo/gxustaj_edit.text)
	konservi_malgxustajn_vortojn($HBoxContainer2/malgxustaj_vortoj_agordo/malgxustaj_edit.text)
