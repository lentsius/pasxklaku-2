extends Control

func _ready():
	gxisdatigxi()
	set_process(false)

func _process(delta):
	if Input.is_action_just_pressed("pauzo"):
		reveni()

func gxisdatigxi():
	$fin_menuo/REVENI.grab_focus()
	
func tradukigxi():
	var tr = TranslationServer
	$fin_menuo/REVENI.text = tr.translate("REVENI")
	$fin_menuo/menuo.text = tr.translate("CXEFA MENUO")
	$fin_menuo/fermi.text = tr.translate("FERMI")

func _on_REVENI_pressed():
	reveni()
	
func _on_menuo_pressed():
	get_tree().paused = false
	nuligi_poentojn()
	get_tree().change_scene_to(preload("res://scenoj/menuo.tscn"))
	
func _on_fermi_pressed():
	get_tree().quit()

func reveni():
	self.visible = false
	get_tree().paused = false
	set_process(false)
	get_parent().sxalti_muzikon()
	
func nuligi_poentojn():
	globala.l1_poentoj = 0
	globala.l2_poentoj = 0
