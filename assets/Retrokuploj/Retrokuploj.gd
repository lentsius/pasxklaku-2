extends ColorRect

var nevidebla : Color = Color(1, 1, 1, 0)
var videbla : Color = Color(1, 1, 1)

func _ready() -> void:
	$Malheleco.modulate = nevidebla
	set_process_input(false)
	set_process(false)
	visible = false

func _input(event):
	if event is InputEventKey or event is InputEventMouseButton:
		malaperi(true)
		set_process_input(false)
		
func _process(delta) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		malaperi()

func sendi_peton(url, data_to_send, use_ssl):
	# Convert data to json string:
	var query = JSON.print(data_to_send)
	# Add 'Content-Type' header:
	var headers = ["Content-Type: application/json"]
	$HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	if response_code == 200:
		print("Sukcese sendis retrokuplon!")
		aperigi_mesagxon(Color.white, TranslationServer.translate("RETROKUPLO_SENDITA"), true)
	else:
		aperigi_mesagxon(Color.white, TranslationServer.translate("UPS_RETROKUPLO"), false)
		
		
func aperi() -> void:
	set_process(true)
	$ColorRect/VBoxContainer/H1/Retrokuplo.text = TranslationServer.translate("METU_TEKSTON")
	visible = true
	$Malheleco.modulate.a = 0
	$Tvino.interpolate_property(self, 'modulate', nevidebla, videbla, 2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	
func malaperi(fast : bool = false) -> void:
	set_process(false)
	if !fast:
		$Tvino.interpolate_property(self, 'modulate', videbla, nevidebla, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
		$Tvino.start()
		yield($Tvino, "tween_completed")
		
	else:
		$Malheleco.modulate = nevidebla
		modulate = nevidebla

	$Tvino.stop_all()
	$Malheleco.modulate.a = 0
	visible = false
	get_parent().unua_butono.grab_focus()

func _on_Sendi_pressed():
	#unue ni kontrolu ĉu la uzanto bone enskribis la retrokuplon
	var permeso : Array = []
	if $ColorRect/VBoxContainer/H2/Adreso.text.find("@") > 0 && $ColorRect/VBoxContainer/H2/Adreso.text.find(".") > 0:
		printt("has the at and a fullstop symbol!")
		permeso.append(true)
	else:
		permeso.append(false)
	
	if $ColorRect/VBoxContainer/H1/Retrokuplo.text != "":
		permeso.append(true)
	else:
		permeso.append(false)
	
	# se ĉio estis bone enskribita ni sendu la peton	
	if permeso[1] == true && permeso[0] == true:
		print("SENDING THE INFO BOYS!")
		var dict : Dictionary = {"email" : $ColorRect/VBoxContainer/H2/Adreso.text, "mesagxo" : $ColorRect/VBoxContainer/H1/Retrokuplo.text, "versio" : str(ProjectSettings.get_setting("application/config/version")), "nivelo" : ProjectSettings.get_setting("application/config/nivelo"), "lingvo" : globala.lingvo_vorto}
		sendi_peton("https://vortoj-retrokuplo.firebaseio.com/retrokuploj.json", dict, false)
	
	# se ne...
	else:
		print(permeso)
		match permeso:
			[false, false]:
				print("ambaŭ estis malbone enskribitaj")
				aperigi_mesagxon(Color.coral, TranslationServer.translate("ERARO_AMBAU"))
			[true, false]:
				print("lla teksto estis malĝuste enmetita")
				aperigi_mesagxon(Color.coral, TranslationServer.translate("ERARO_TEKSTO"))
			[false, true]:
				print("nur retpoŝadreso estis malĝuste metita")
				aperigi_mesagxon(Color.coral, TranslationServer.translate("ERARO_RETPOSXTADRESO"))

func _on_Reen_pressed():
	malaperi()

func aperigi_mesagxon(koloro : Color, teksto : String, final : bool = false) -> void:
	$Malheleco/Teksto.modulate = koloro
	$Malheleco/Teksto.text = teksto
	$Tvino.interpolate_property($Malheleco, 'modulate', nevidebla, videbla, 1.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	if !final:
		$Tvino.interpolate_property($Malheleco, 'modulate', videbla, nevidebla, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 1.5)
	else:
		set_process_input(true)
	
	$Tvino.start()

func _on_RetrokuploButono_pressed():
	aperi()


func _on_Retrokuplo_focus_entered():
	if $ColorRect/VBoxContainer/H1/Retrokuplo.text == TranslationServer.translate("METU_TEKSTON"):
		$ColorRect/VBoxContainer/H1/Retrokuplo.text = ""

func _on_Retrokuplo_focus_exited():
	if $ColorRect/VBoxContainer/H1/Retrokuplo.text == "":
		$ColorRect/VBoxContainer/H1/Retrokuplo.text = TranslationServer.translate("METU_TEKSTON")
