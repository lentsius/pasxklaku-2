extends HBoxContainer

onready var lingvo_sxablono : PackedScene = preload("res://assets/lingvoj/lingvo_sxablono.tscn")

const lingvoj_dosiervojo : String = "res://vortoj/"

var lingvoj : Array = []

signal lingvoj_pretas(kiom)

func _ready():
	#malkovri kiom da lingvoj estas
	var lingva_dosierujo = Directory.new()
	lingva_dosierujo.open(lingvoj_dosiervojo)
	
	lingva_dosierujo.list_dir_begin(true, true)
	var legata_lingvo = lingva_dosierujo.get_next()
	lingvoj.append(legata_lingvo)
	while legata_lingvo != "":
		legata_lingvo = lingva_dosierujo.get_next()
		if legata_lingvo != "":
			lingvoj.append(legata_lingvo)
	
#	print(lingvoj)
	lingva_dosierujo.list_dir_end()
	
	for i in lingvoj:
		var nova_lingvo = lingvo_sxablono.instance()
		nova_lingvo.modulate = Color('32ffffff')
		nova_lingvo.text = i.to_upper()
		nova_lingvo.name = i
		add_child(nova_lingvo)
		
#	print(lingvoj.size())
	
	get_children()[0].modulate = Color(1,1,1,1)
	emit_signal("lingvoj_pretas", lingvoj.size())
