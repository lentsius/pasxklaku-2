extends ColorRect

signal kreis_novan_kategorion(nomo)

var malpermesite : Array = []

func _ready() -> void:
	visible = false

func aperi() -> void:
	$Ekrano/VBox/NomoDeNova.hint_tooltip = TranslationServer.translate("NOMO_DE_NOVA")
	$Ekrano/VBox/NomoDeNova.placeholder_text = TranslationServer.translate("NOMO_DE_NOVA")
	$Ekrano/VBox/NomoDeNova.clear()
	$Tvino.interpolate_property(self, 'modulate:a', 0, 1, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	visible = true


func malaperi() -> void:
	$Ekrano/VBox/NomoDeNova.clear()
	modulate.a = 0
	visible = false
	

func _on_Aldoni_pressed() -> void:
	aperi()

func _on_Reen_pressed():
	malaperi()
	
func _on_Konfirmi_pressed():
	if $Ekrano/VBox/NomoDeNova.text == "" || !$Ekrano/VBox/NomoDeNova.text.is_valid_filename():
		return
	krei_novan(globala.redaktata_lingvo)
	malaperi()
	
	
func krei_novan(alia : String = ""):
	var ujo = Directory.new()
	var dosiero = File.new()
	
	var lingvo : String = globala.lingva_dosierujo
	if alia != "":
		lingvo = alia
	
	#unue ni kreu la bazan dosieron
	ujo.make_dir(globala.lingva_dosierujo + lingvo + "/" + $Ekrano/VBox/NomoDeNova.text.to_lower())
	print("kreas kategorion por lingvo: ", lingvo)
	#aldoni ni kreu malplenajn dosierojn por la vortoj
	dosiero.open(globala.lingva_dosierujo + globala.lingvo_vorto + "/" + $Ekrano/VBox/NomoDeNova.text.to_lower() + "/" + "malgxustaj.txt", File.WRITE)
	dosiero.open(globala.lingva_dosierujo + globala.lingvo_vorto + "/" + $Ekrano/VBox/NomoDeNova.text.to_lower() + "/" + "gxustaj.txt", File.WRITE)
	
	emit_signal("kreis_novan_kategorion", $Ekrano/VBox/NomoDeNova.text.to_lower())
	

func _on_NomoDeNova_text_changed(new_text: String) -> void:
	if new_text == "":
		return 
		
	if !new_text.is_valid_filename():
		$Ekrano/VBox/HBoxContainer/Konfirmi.disabled = true
		$Ekrano/VBox/HBoxContainer2/Netaugaj.visible = true
	else:
		$Ekrano/VBox/HBoxContainer/Konfirmi.disabled = false
		$Ekrano/VBox/HBoxContainer2/Netaugaj.visible = false
