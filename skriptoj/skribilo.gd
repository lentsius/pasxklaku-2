extends Node

var alfabeto = []
var kursoro = 0
var teksto = TranslationServer.translate("VIA NOMO") # komenca teksto

func _ready() -> void:
	gxisdatigi_alfabeton()
		
func supresube(supre) -> void:
	# sxangxas la leteron je la kursoro kun la venonta (se supre = 1)
	# aux kun la antauxa (se supre = -1) laux la alfabeto
	var letero = self.teksto.substr(self.kursoro, 1)
	var nova_letero_index
	if len(alfabeto) > 0:
		nova_letero_index = (alfabeto.find(letero) + supre) % len(alfabeto)
	var nova_letero = alfabeto[nova_letero_index]
	
	teksto = self.teksto.substr(0, self.kursoro) + \
		nova_letero + \
		self.teksto.substr(self.kursoro + 1, len(self.teksto))
	
func dekstremaldekstre(dekstre) -> void:
	# igas antauxen/malantauxen la kursoron
	self.kursoro += dekstre
	# limigas la maksimumon je la longeco de la teksto
	self.kursoro = min(self.kursoro, 24)
	# limigas la minimumon je nulo
	self.kursoro = max(self.kursoro, 0)
	
func nuligxi() -> void:
	self.teksto = ""
	self.kursoro = 0
	
func forigi_lastan() -> void:
	var longeco = teksto.length()
	teksto.erase(longeco-1, 1)
	if kursoro >= longeco:
		kursoro = longeco - 1
		
func gxisdatigi_alfabeton() -> void:
	alfabeto = globala.lingva_alfabeto.duplicate()
