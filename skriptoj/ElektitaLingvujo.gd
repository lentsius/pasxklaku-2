extends Control

var origina_pozicio_nova : Vector2

var distanco : int = 200

onready var lingvujo : VBoxContainer = $VBoxContainer



func _ready():
	origina_pozicio_nova = lingvujo.rect_position
	aktualigi_partiklajn_fortecon()

#func _process(delta) -> void:
#	if !AudioServer.is_bus_mute(1):
#		var lauteco = abs(AudioServer.get_bus_peak_volume_left_db(0, 0))
#		print(lauteco)
#		$Linio.width = (lauteco * lauteco * 2.2) / 5
#		$Linio.modulate.a = -lauteco / 50
#	else:
#		$Linio.modulate.a = 0

func aktualigi_lingvon() -> void:
	if $Tvino.is_active():
		$Tvino.stop_all()
		lingvujo.modulate = Color(1,1,1,0)
	var komenca : Vector2 = Vector2(origina_pozicio_nova.x, origina_pozicio_nova.y + distanco)
	
	#Tvino por la nova lingvo
	lingvujo.modulate = Color(1,1,1,0)
	$Tvino.interpolate_property(lingvujo, "rect_position", komenca, origina_pozicio_nova, 1, Tween.TRANS_ELASTIC, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property(lingvujo, "modulate", Color(1,1,1,0), Color(1,1,1,1), 0.8, Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
	$Tvino.start()
	
func aktualigi_partiklajn_fortecon() -> void:
	print(globala.efektokvanto)
	$Partikloj.self_modulate.a = globala.efektokvanto
	$Partikloj2.self_modulate.a = globala.efektokvanto

func _on_kvanto_value_changed(value):
	aktualigi_partiklajn_fortecon()
