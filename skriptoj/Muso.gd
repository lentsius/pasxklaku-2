extends Node

var muso : bool = false

func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event) -> void:
	if event is InputEventMouse and muso == false:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		muso = true
		$Tempigilo.start()
		
	if event is InputEventMouse and muso == true:
		$Tempigilo.start()

func _on_Tempigilo_timeout():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$Tempigilo.stop()
	muso = false
