extends Control

var orig_poz : Vector2

const pagxo_de_lentsius : String = "https://lentsius-bark.itch.io/"
const pagxo_de_muziko : String = "https://soundcloud.com/4bstr4ck3r"
const pagxo_de_ikso_net : String = "https://ikso.net/"
const pagxo_de_ludmotoro : String = "https://godotengine.org/"

var aktiva : = false


func _ready() -> void:
	visible = false
	orig_poz = $Menufono.rect_position
	$Menufono/Mencioj.modulate.a = 0


func eniri() -> void:
	aktiva = true
	set_process_input(true)
	$Sxovo.play()
	$Tvino.stop_all()
	$Tvino.interpolate_property($Menufono, 'rect_scale:x',0.01, 1, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($AgordFono, 'modulate', Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($Menufono/Mencioj, 'modulate:a', 0, 1, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	visible = true
	

func eliri() -> void:
	aktiva = false
	set_process_input(false)
	$Sxovo.play()
	
	$Tvino.stop_all()
	$Tvino.interpolate_property($AgordFono, 'modulate:a', 1, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0.0)
	$Tvino.interpolate_property($Menufono, 'rect_scale:x',1, 0.01, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.interpolate_property($Menufono/Mencioj, 'modulate:a', 1, 0, 0.15, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	get_parent().unua_butono.grab_focus()	
	get_parent().centrigi_emblemon()
	get_parent().kreditoj = false
	
	yield($Tvino, 'tween_completed')
	
	self.visible = false
	
	
func _input(event) -> void:
	if event is InputEventJoypadButton or event is InputEventKey:
		if get_parent().kreditoj and aktiva:
			eliri()


func _on_muziko_butono_pressed():
	get_parent().peti_iri_al_pagxo(pagxo_de_muziko)


func _on_ludkreo_butono_pressed():
	get_parent().peti_iri_al_pagxo(pagxo_de_lentsius)


func _on_organizo_butono_pressed():
	get_parent().peti_iri_al_pagxo(pagxo_de_ikso_net + globala.lingvo_vorto)


func _on_ludmotoro_butono_pressed():
	get_parent().peti_iri_al_pagxo(pagxo_de_ludmotoro)


func _on_AgordFono_gui_input(event):
	if event is InputEventMouseButton and aktiva:
		eliri()

