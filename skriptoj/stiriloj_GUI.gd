extends Control

export(int) var ludanto = 1
export(bool) var gvid_stiriloj = false

func _ready() -> void:
	if globala.helpaj_regiloj:
		$h/Jeso.visible = true
		$h/Neo.visible = true
		$h/v/Demando.visible = true
		$h/v/Demando2.visible = true

func _process(delta):
	if ludanto == 1:
		if Input.is_action_pressed("up"):
			$h/v/antaue.color = Color(1,1,1,1)
		else:
			$h/v/antaue.color = Color(1,1,1,0.1)

		if Input.is_action_pressed("down"):
			$h/v/malantaue.color = Color(1,1,1,1)
		else:
			$h/v/malantaue.color = Color(1,1,1,0.1)
			
		if Input.is_action_pressed("left"):
			$h/c/maldekstre.color = Color(1,1,1,1)
		else:
			$h/c/maldekstre.color = Color(1,1,1,0.1)
			
		if Input.is_action_pressed("right"):
			$h/c2/dekstre.color = Color(1,1,1,1)
		else:
			$h/c2/dekstre.color = Color(1,1,1,0.1)
			
	if ludanto == 2:
		if Input.is_action_pressed("up_l2"):
			$h/v/antaue.color = Color(1,1,1,1)
		else:
			$h/v/antaue.color = Color(1,1,1,0.1)

		if Input.is_action_pressed("down_l2"):
			$h/v/malantaue.color = Color(1,1,1,1)
		else:
			$h/v/malantaue.color = Color(1,1,1,0.1)
			
		if Input.is_action_pressed("left_l2"):
			$h/c/maldekstre.color = Color(1,1,1,1)
		else:
			$h/c/maldekstre.color = Color(1,1,1,0.1)
			
		if Input.is_action_pressed("right_l2"):
			$h/c2/dekstre.color = Color(1,1,1,1)
		else:
			$h/c2/dekstre.color = Color(1,1,1,0.1)
