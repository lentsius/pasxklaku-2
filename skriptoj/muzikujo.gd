extends Control

func _ready():
	var muzik_lauteco = globala.sxargxi_agordon("agordoj", "muzik_lauteco", 100)
	if muzik_lauteco == 0:
		$vbox/muzikujo/muziko.pressed = false
	$vbox/muzikujo/lauxteco.value = muzik_lauteco
	$vbox/muzikujo/lauxteco_numero.text = str(muzik_lauteco)
	
	var efekt_lauteco = globala.sxargxi_agordon("agordoj", "efekt_lauteco", 100)
	if efekt_lauteco == 0:
		$vbox/sonefektujo/sonefektoj.pressed = false
	$vbox/sonefektujo/lauxteco_numero.text = str(efekt_lauteco)
	$vbox/sonefektujo/lauxteco_de_efektoj.value = efekt_lauteco

	
	
func _on_lauxteco_value_changed(value):
	$vbox/muzikujo/lauxteco_numero.text = str(value)
	var db = konverti_db(value)
	if value == 0:
		AudioServer.set_bus_mute(1, true)
		$vbox/muzikujo/muziko.pressed = false
	else:
		AudioServer.set_bus_mute(1, false)
		$vbox/muzikujo/muziko.pressed = true
	AudioServer.set_bus_volume_db(1, db)
	globala.konservi_agordon("agordoj", "muzik_lauteco", value)
	ludi_klakon(2)
	
	
func _on_lauxteco_de_efektoj_value_changed(value):
	$vbox/sonefektujo/lauxteco_numero.text = str(value)
	var db = konverti_db(value)
	if value == 0:
		AudioServer.set_bus_mute(2, true)
		$vbox/sonefektujo/sonefektoj.pressed = false
	else:
		AudioServer.set_bus_mute(2, false)
		$vbox/sonefektujo/sonefektoj.pressed = true		
	AudioServer.set_bus_volume_db(2, db)
	globala.konservi_agordon("agordoj", "efekt_lauteco", value)	
	ludi_klakon(3)
	
	
func konverti_db(val):
	var db = val - 100
	if db != 0:
		db = db / 8
	
	return db
	
	
func ludi_klakon(val):
	var klako = get_node("klako_" + str(val))
	klako.play(0.0)
	
	
func _on_muziko_toggled(button_pressed):
	if button_pressed:
		$vbox/muzikujo/lauxteco_numero.text = "100"
		AudioServer.set_bus_mute(1, false)
		AudioServer.set_bus_volume_db(1, 0)
		globala.konservi_agordon("agordoj", "muzik_lauteco", 100)
		$vbox/muzikujo/lauxteco.value = 100
		ludi_klakon(2)
	else:
		$vbox/muzikujo/lauxteco_numero.text = "0"
		AudioServer.set_bus_mute(1, true)
		globala.konservi_agordon("agordoj", "muzik_lauteco", 0)
		$vbox/muzikujo/lauxteco.value = 0
		ludi_klakon(2)


func _on_sonefektoj_toggled(button_pressed):
	if button_pressed:
		$vbox/sonefektujo/lauxteco_numero.text = "100"
		AudioServer.set_bus_mute(2, false)
		AudioServer.set_bus_volume_db(2, 0)
		globala.konservi_agordon("agordoj", "efekt_lauteco", 100)
		$vbox/sonefektujo/lauxteco_de_efektoj.value = 100
		ludi_klakon(2)
	else:
		$vbox/sonefektujo/lauxteco_numero.text = "0"
		AudioServer.set_bus_mute(2, true)
		globala.konservi_agordon("agordoj", "efekt_lauteco", 0)
		$vbox/sonefektujo/lauxteco_de_efektoj.value = 0
		ludi_klakon(2)

func _on_sonefektoj_pressed():
	pass # Replace with function body.
