extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Ujo/Lingvujo/Lingvo.text = TranslationServer.tr("LINGVA_NOMO")
	$Ujo/Lingvujo2/Malfacileco.text = TranslationServer.tr("MALFACILECO_" + globala.malfacileco_vorto.to_upper())
	$Flago.texture = globala.preni_flageton()
