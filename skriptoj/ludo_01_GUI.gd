extends Control

onready var l1_poentoj = $maldekstre/poentoj
onready var l2_poentoj = $dekstre/poentoj

var poentoj_l1_kalk = 0
var poentoj_l2_kalk = 0

### !!! SUBE ESTAS TREM_VARIABLOJ

var origin_maldesktra_pozicio : Vector2
var origin_desktra_pozicio : Vector2

var tremas_dekstren = false
var tremas_maldekstren = false

var trem_forteco = 1

var trem_longeco = 1

### !!! SUPRE ESTAS TREM_VARIABLOJ

func _ready():
	origin_desktra_pozicio = $dekstre.rect_position
	origin_maldesktra_pozicio = $maldekstre.rect_position
	globala.gui = self
	$lud_tempo.start()
#	tradukigxi()
	agordigi()
	
func agordigi() -> void:
	var kvanto = globala.ui_kvanto
	$maldekstre.modulate.a = kvanto
	$dekstre.modulate.a = kvanto

func _process(delta):
	gxisdatigi_tempon()
	gxisdatigi_poentojn()
	kontroli_tempon()
	
	if tremas_dekstren:
		tremigi_dekstran()
	if tremas_maldekstren:
		tremigi_maldekstran()
	
func gxisdatigi_poentojn():
	l1_poentoj.text = str(floor(poentoj_l1_kalk))
	l2_poentoj.text = str(floor(poentoj_l2_kalk))
	
func gxisdatigi_tempon():
	if $lud_tempo.time_left != 0:
		$tempo.text = str(ceil($lud_tempo.time_left))


func kontroli_tempon():
	if $lud_tempo.time_left < 60 and $lud_tempo.time_left > 30:
		$tempo.modulate = Color(1,1,1)
		$tempo_animacio.playback_speed = 0.5
	if $lud_tempo.time_left < 30 and $lud_tempo.time_left > 10:
		$tempo_animacio.playback_speed = 1
		$tempo.modulate = Color(1,0.5,0)

	if $lud_tempo.time_left < 10:
		$tempo.modulate = Color(1,0,0)
		$tempo_animacio.playback_speed = 2
		
### anoncoj pro batoj
func mistrafis(ludanto) -> void:
	if ludanto == 1:
		$'../sonoj/ooo_l1'.play()
		l1_videbligi($maldekstre/centro/mistrafis)
		yield(get_tree().create_timer(1), "timeout")
		l1_malvidebligi($maldekstre/centro/mistrafis)
	else:
		$'../sonoj/ooo_l2'.play()
		l2_videbligi($dekstre/centro/mistrafis)
		yield(get_tree().create_timer(1), "timeout")
		l2_malvidebligi($dekstre/centro/mistrafis)
	
func ll1_frue():
	gxisdatigi_l1_poentojn()
#	$'../sonoj/frue_l1'.play()
	$'../sonoj/sono_frue_l1'.play()
	l1_videbligi($maldekstre/centro/frue)
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/frue)


	
func ll1_bone():
	gxisdatigi_l1_poentojn()	
#	$'../sonoj/bone_l1'.play()
	$'../sonoj/sono_bone_l1'.play()
	if globala.tremoj:
		tremigi(1, 5, 1.5)
	l1_videbligi($maldekstre/centro/bone)
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/bone)


	
func ll1_mojose():
	gxisdatigi_l1_poentojn()	
#	$'../sonoj/mojose_l1'.play()
	$'../sonoj/manklakoj_l1'.play()
	$'../sonoj/sprinkle_l1'.play()
#	$'../kamerao/kamerao_animacioj'.play("tremo")
	l1_videbligi($maldekstre/centro/mojose)
	### TREMOJ
	if globala.tremoj:
		tremigi(1, 15, 3.0)	
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/mojose)
		
	
func ll2_frue():
	gxisdatigi_l2_poentojn()
#	$'../sonoj/frue_l2'.play()
	$'../sonoj/sono_frue_l2'.play()
	l2_videbligi($dekstre/centro/frue)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/frue)
		
func ll2_bone():
	gxisdatigi_l2_poentojn()	
#	$'../sonoj/bone_l2'.play()
	$'../sonoj/sono_bone_l2'.play()
	if globala.tremoj:
		tremigi(2, 5, 1.5)
	l2_videbligi($dekstre/centro/bone)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/bone)

func ll2_mojose():
	gxisdatigi_l2_poentojn()	
#	$'../sonoj/mojose_l2'.play()
	$'../sonoj/manklakoj_l2'.play()
	$'../sonoj/sprinkle_l2'.play()
	### TREMOJ
	if globala.tremoj:
		tremigi(2, 15, 3.0)
	l2_videbligi($dekstre/centro/mojose)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/mojose)
		
func _on_lud_tempo_timeout():
	$tempo.text = TranslationServer.translate("PUNKTO_FINO")
	$tempo_animacio.play("grandigxi")
#	$'../sonoj/punktofino'.play()
	
func l1_malvidebligi(objekto):
	$maldekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,1), Color(1,1,1,0), 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$maldekstre/centro/tvino.start()

func l1_videbligi(objekto):
	$maldekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$maldekstre/centro/tvino.start()

func l2_malvidebligi(objekto):
	$dekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,1), Color(1,1,1,0), 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$dekstre/centro/tvino.start()

func l2_videbligi(objekto):
	$dekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$dekstre/centro/tvino.start()
	
func gxisdatigi_kombon(ludanto, kombo):
	if ludanto == 1:
		$maldekstre/kombo_skatulo2/kombnumero.text = str(kombo)
	else:
		$dekstre/kombo_skatulo/kombnumero.text = str(kombo)
		
func tradukigxi():
	$dekstre/centro/mistrafis.text = TranslationServer.translate("MISTRAFIS")
	$dekstre/centro/bone.text = TranslationServer.translate("BONE")
	$dekstre/centro/frue.text = TranslationServer.translate("FRUE")
	$dekstre/centro/mojose.text = TranslationServer.translate("MOJOSE")
#	$dekstre/l2.text = TranslationServer.translate("L2")
	$dekstre/kombo_skatulo/kombo.text = TranslationServer.translate("KOMBO")
	$maldekstre/centro/mistrafis.text = TranslationServer.translate("MISTRAFIS")
	$maldekstre/centro/bone.text = TranslationServer.translate("BONE")
	$maldekstre/centro/frue.text = TranslationServer.translate("FRUE")
	$maldekstre/centro/mojose.text = TranslationServer.translate("MOJOSE")	
#	$maldekstre/l1.text = TranslationServer.translate("L1")
	$maldekstre/kombo_skatulo2/kombo.text = TranslationServer.translate("KOMBO")
	
	# por la rusa necesas sxangxi la tiparon de tekstoj	
	if globala.lingvo_vorto == "ru":
		#unue la tiparo
		var rusa_tiparo = load('res://assets/vorto/vidpunkto_tiparo_rusa.tres')
		#diversajxoj
		$dekstre/l2.add_font_override("font", rusa_tiparo)
		$maldekstre/l1.add_font_override("font", rusa_tiparo)
		#krioj!
		for i in $dekstre/centro.get_children():
			if !i is Tween:
				i.add_font_override("font", rusa_tiparo)
		for i in $maldekstre/centro.get_children():
			if !i is Tween:
				i.add_font_override("font", rusa_tiparo)			
			
func gxisdatigi_l1_poentojn():
	$maldekstre/poent_tvino.interpolate_property(self, "poentoj_l1_kalk", poentoj_l1_kalk, globala.l1_poentoj, 5, Tween.TRANS_ELASTIC, Tween.EASE_OUT, 0)
	$maldekstre/poent_tvino.start()
	
func gxisdatigi_l2_poentojn():
	$dekstre/poent_tvino.interpolate_property(self, "poentoj_l2_kalk", poentoj_l2_kalk, globala.l2_poentoj, 5, Tween.TRANS_ELASTIC, Tween.EASE_OUT, 0)
	$dekstre/poent_tvino.start()
	
func tremigi(ludanto : int, forteco : int, longeco : float = trem_longeco):
	if ludanto == 1:
		tremas_maldekstren = true
	elif ludanto == 2:
		tremas_dekstren = true		
	trem_forteco = forteco
	$tvino.interpolate_property(self, 'trem_forteco', trem_forteco, 0, longeco, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 0)
	$tvino.start()
	
func tremigi_maldekstran():
	var hazardnumero = rand_range(-trem_forteco, trem_forteco)
	$maldekstre.rect_position.x = origin_maldesktra_pozicio[0] + hazardnumero
	$maldekstre.rect_position.y = origin_maldesktra_pozicio[1] + hazardnumero

func tremigi_dekstran():
	var hazardnumero = rand_range(-trem_forteco, trem_forteco)
	$dekstre.rect_position.x = origin_desktra_pozicio[0] + hazardnumero
	$dekstre.rect_position.y = origin_desktra_pozicio[1] + hazardnumero	

func _on_tvino_tween_completed(object, key):
	if tremas_dekstren:
		tremas_dekstren = false
	if tremas_maldekstren:
		tremas_maldekstren = false
