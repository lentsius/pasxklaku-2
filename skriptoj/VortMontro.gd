extends ScrollContainer

onready var montrovorto = preload('res://assets/vorto/montrovorto.tscn')

var scroll : int = 1
var scrollbar : ScrollBar

func _ready() -> void:
	set_process(false)
	scrollbar = get_v_scrollbar()
	print(scrollbar.max_value)


func sxaltigxi() -> void:
	set_process(true)


func malsxaltigxi() -> void:
	set_process(false)
		
	
func _process(delta):
	scroll_vertical += scroll
#	print(scrollbar.value, " ma value : " , scrollbar.max_value)
#	print(scroll_vertical)
#	print(scroll_deadzone)

func _on_VortMontro_scroll_ended():
	print("scroll ended!")

func vortigxi(novaj : Array = []) -> void:
	for i in $VortListo.get_children():
		i.queue_free()
		
	for i in novaj:
		var nova_vorto = montrovorto.instance()
		nova_vorto.text = i
		$VortListo.add_child(nova_vorto)
		
	
