extends Control

var neaktiva = Color("2cffffff")
var aktiva = Color("ffffffff")

var ludanto = 1
export var ambaux = false
export var entajpi = false
export(int, 1, 2) var ludanto_montri : int = 1

func _ready():
	ludanto = ludanto_montri
	
	if entajpi:
		$montrujo/Label2.visible = false


func _process(delta):
	var ludanta_sufikso = ""
	if ludanto == 2:
		ludanta_sufikso = "_l2"
		
	if !ambaux:
		unuludante(ludanta_sufikso)
	else:
		ambaux_fari()
		
func unuludante(ludanta_sufikso):
	if Input.is_action_pressed("up" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/centrujo/a.modulate = aktiva
	else: 
		$montrujo/duujo/klakbutonoj/centrujo/a.modulate = neaktiva
		
	if	Input.is_action_pressed("down" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/centrujo/s.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/centrujo/s.modulate = neaktiva

	if	Input.is_action_pressed("left" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/maldesktrujo/md.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/maldesktrujo/md.modulate = neaktiva
	
	if	Input.is_action_pressed("right" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/dekstrujo/d.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/dekstrujo/d.modulate = neaktiva

	if	Input.is_action_pressed("left_down" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/maldesktrujo/mds.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/maldesktrujo/mds.modulate = neaktiva
	
	if	Input.is_action_pressed("right_down" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/dekstrujo/ds.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/dekstrujo/ds.modulate = neaktiva
		
	if	Input.is_action_pressed("right_up" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/dekstrujo/dsupre.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/dekstrujo/dsupre.modulate = neaktiva
		
	if	Input.is_action_pressed("left_up" + ludanta_sufikso):
		$montrujo/duujo/klakbutonoj/maldesktrujo/mdsupre.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/maldesktrujo/mdsupre.modulate = neaktiva

func ambaux_fari():
	if Input.is_action_pressed("up") or Input.is_action_pressed("up_l2"):
		$montrujo/duujo/klakbutonoj/centrujo/a.modulate = aktiva
	else: 
		$montrujo/duujo/klakbutonoj/centrujo/a.modulate = neaktiva
		
	if	Input.is_action_pressed("down") or Input.is_action_pressed("down_l2"):
		$montrujo/duujo/klakbutonoj/centrujo/s.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/centrujo/s.modulate = neaktiva

	if	Input.is_action_pressed("left") or Input.is_action_pressed("left_l2"):
		$montrujo/duujo/klakbutonoj/maldesktrujo/md.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/maldesktrujo/md.modulate = neaktiva
	
	if	Input.is_action_pressed("right") or Input.is_action_pressed("right_l2"):
		$montrujo/duujo/klakbutonoj/dekstrujo/d.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/dekstrujo/d.modulate = neaktiva

	if	Input.is_action_pressed("left_down") or Input.is_action_pressed("left_down_l2"):
		$montrujo/duujo/klakbutonoj/maldesktrujo/mds.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/maldesktrujo/mds.modulate = neaktiva
	
	if	Input.is_action_pressed("right_down") or Input.is_action_pressed("right_down_l2"):
		$montrujo/duujo/klakbutonoj/dekstrujo/ds.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/dekstrujo/ds.modulate = neaktiva
		
	if	Input.is_action_pressed("right_up") or Input.is_action_pressed("right_up_l2"):
		$montrujo/duujo/klakbutonoj/dekstrujo/dsupre.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/dekstrujo/dsupre.modulate = neaktiva
		
	if	Input.is_action_pressed("left_up") or Input.is_action_pressed("left_up_l2"):
		$montrujo/duujo/klakbutonoj/maldesktrujo/mdsupre.modulate = aktiva
	else:
		$montrujo/duujo/klakbutonoj/maldesktrujo/mdsupre.modulate = neaktiva

func tradukigxi():
	var ts = TranslationServer
	$montrujo/litero_supren.text = ts.translate("LITERO_SUPREN")
	$montrujo/duujo/Maldekstren.text = ts.translate("MALDEKSTREN")
	$montrujo/duujo/dekstren.text = ts.translate("DEKSTREN")
	$montrujo/triujo/nuligi.text = ts.translate("NULIGI")
	$montrujo/triujo/nuligi_lastan.text = ts.translate("FORIGI_LASTAN")
	$montrujo/triujo/litero_suben.text = ts.translate("LITERO_SUBEN")
	
func _input(event):
#	if event is InputEventKey:
#		print(OS.get_scancode_string(event.scancode))
	if event is InputEventJoypadButton:
		print(event.button_index)
