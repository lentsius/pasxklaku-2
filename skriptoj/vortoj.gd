extends Node2D

#var mia_fono

var gxustaj_vortoj : = []
var malgxustaj_vortoj : = []
var volapukajxoj : = []

var uzitaj_gxustaj_vortoj : = []
var uzitaj_malgxustaj_vortoj : = []
var uzitaj_volapukajxoj : = []

var novvorta_rapideco = 170

var finlisto : = []

var nuna_vorto : = 0

var nombro_da_vortoj : = 0

var vorteblas : = true

var vortoj_en_la_ludo : = 0

var tween_agordoj = [Color(1,1,1,0), Color(1,1,1,1)]
var jam_ludis_tween : = false

var alfabeto : Array = []

# la nombro a vortoj por preni por la ludo
var vortoj_por_preni : int = 35

#variablo pos scii kion fari post la pauxzo
#var pauxzmodo = ""

## unue ni kontrolos kian lingvon la uzanto selektis
## uzante la variablon lingva_dosierujo
## kiu supozas esperanton unue

var lingva_dosierujo = ""

func _ready():
	print("LA SANCOJ ESTAS:" , globala.gxustsxanco, "kaj:",  globala.malgxustsxanco, "kaj fine:", globala.volapuksxanco)
	lingva_dosierujo = "user://lingvoj/" + globala.lingvo_vorto + "/"
	
#	randomize()
	gxustaj_vortoj = preni_gxustajn_vortojn()
	limigi_gxustajn_vortojn(vortoj_por_preni)
	
	malgxustaj_vortoj = preni_malgxustajn_vortojn()
	limigi_malgxustajn_vortojn(vortoj_por_preni)
	
#	volapukajxoj = preni_volapukajxojn()
#	limigi_volapukajxojn(30)
		
	volapukajxoj = krei_volapukajxojn(vortoj_por_preni)
#	print(volapukajxoj)
		
	#ni kalkulu la sumon por scii kiom granda la fina listo estos
	nombro_da_vortoj = volapukajxoj.size() + gxustaj_vortoj.size() + malgxustaj_vortoj.size()

	#hazardigi la listojn
	gxustaj_vortoj.shuffle()
	malgxustaj_vortoj.shuffle()
	volapukajxoj.shuffle()

	yield(get_tree(), 'idle_frame')
	#krei la finan liston de vortoj por uzi en la ludo mem
	var sxancoj : Array = [globala.gxustsxanco, globala.malgxustsxanco, globala.volapuksxanco]
	print()
	while not finlisto.size() == nombro_da_vortoj * 2:
#		randomize()
		var hazard_elekto = randf()
		if hazard_elekto > 0 and hazard_elekto < sxancoj[0]:
			print("hazard elekto estis:", hazard_elekto, " kaj malpli alta ol la ŝanco de ne gusta vorto kiu estis: ", sxancoj[0])
			if gxustaj_vortoj.size() == 0:
				break
			finlisto.append(gxustaj_vortoj[0])
			uzitaj_gxustaj_vortoj.append(gxustaj_vortoj[0])			
			gxustaj_vortoj.remove(0)
			finlisto.append(1)
		if hazard_elekto > sxancoj[0] and hazard_elekto < sxancoj[0] + sxancoj[1]:
			print("hazard elekto estis:", hazard_elekto, " kaj malpli alta ol la ŝanco de ne malgxusta vorto kiu estis: ", sxancoj[1])
			if malgxustaj_vortoj.size() == 0:
				break
			finlisto.append(malgxustaj_vortoj[0])
			uzitaj_malgxustaj_vortoj.append(malgxustaj_vortoj[0])
			print("aldonis malĝustan vorton kiu estis: ", malgxustaj_vortoj[0])
			malgxustaj_vortoj.remove(0)
			finlisto.append(2)
		if hazard_elekto > sxancoj[0] + sxancoj[1]:
			print("hazard elekto estis:", hazard_elekto, " kaj malpli alta ol la ŝanco de ne volapukajxo vorto kiu estis: ", sxancoj[2])
			if volapukajxoj.size() == 0:
				break
			finlisto.append(volapukajxoj[0])
			uzitaj_volapukajxoj.append(volapukajxoj[0])
			volapukajxoj.remove(0)
			finlisto.append(3)
	
#	print("La finvortlisto estas: ", finlisto)
	on_start()

func _on_ofteco_timeout():
	if nuna_vorto < nombro_da_vortoj * 2 and vorteblas:
		nova_vorto()

func on_start():
	nova_vorto()
	$ofteco.start()
	$tempo.start()
	$mezo.start()
	$komenco.start()
	$fino.start()
	$punktofino.start()

func nova_vorto():
	var vortsceno = load('res://assets/vorto/vorto.tscn')
	var nova_vorto = vortsceno.instance()
	nova_vorto.teksto = finlisto[nuna_vorto]
	nova_vorto.tipo = finlisto[nuna_vorto + 1]
	nova_vorto.modifi_rapidecon(novvorta_rapideco)
	add_child(nova_vorto)
	nuna_vorto += 2	
	
func _on_tempo_timeout():
	vorteblas = false
	
func kontroli_vortojn():
#	print(vortoj_en_la_ludo)
	if vortoj_en_la_ludo < 1:
		$'../ludo'.fino()

	
### VORT PRENADO
func preni_gxustajn_vortojn():
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/gxustaj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func preni_malgxustajn_vortojn():
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/malgxustaj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func preni_volapukajxojn():
	var dosiero = File.new()
	dosiero.open(lingva_dosierujo + globala.kategorio + "/volapukajxoj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func krei_volapukajxojn(kiom : int = 30):
	gxisdatigi_alfabeton()
	var vortojn : Array = []
	var vorto : String = ""
	var longeco : int = 5
#	var no = 0
	for i in range(0, kiom):
		longeco = rand_range(6.0,12.0)
		vorto = krei_hazardan_vorton(longeco)
		vortojn.append(vorto)
	return vortojn
	
func krei_hazardan_vorton(longeco : int):
	var vorto : String = ""
	for i in range(0, longeco):
		vorto += alfabeto[rand_range(0, alfabeto.size())]
	return vorto
	
	
func limigi_gxustajn_vortojn(numero):
	while gxustaj_vortoj.size() > numero:
#		randomize()
		var hazardelekto = randi() % gxustaj_vortoj.size()
		gxustaj_vortoj.remove(hazardelekto)
	
func limigi_malgxustajn_vortojn(numero):
	while malgxustaj_vortoj.size() > numero:
#		randomize()
		var hazardelekto = randi() % malgxustaj_vortoj.size()
		malgxustaj_vortoj.remove(hazardelekto)
	
func limigi_volapukajxojn(numero):
	while volapukajxoj.size() > numero:
#		randomize()
		var hazardelekto = randi() % volapukajxoj.size()
		volapukajxoj.remove(hazardelekto)
		
		
#agordoj sxangxigxas dum la ludo

func _on_komenco_timeout():
	if globala.malfacileco == 1:
		$ofteco.wait_time = 2
		novvorta_rapideco = 180
	elif globala.malfacileco == 2:
		$ofteco.wait_time = 1.5
		novvorta_rapideco = 250

func _on_mezo_timeout():
	if globala.malfacileco == 1:
		vorteblas = false
		$pauzo.start()
		$ofteco.wait_time = 1.75
		novvorta_rapideco = 250
	elif globala.malfacileco == 2:
		vorteblas = false
		$pauzo.start()
		$ofteco.wait_time = 1.25
		novvorta_rapideco = 325

func _on_fino_timeout():
	$Tween.interpolate_property($avertkoloro, "modulate", tween_agordoj[0], tween_agordoj[1], 1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT, 0)
	$Tween.start()
	$avertkoloro.visible = true
	$alarmo.play()
	$pauzo.start()
	vorteblas = false

func _on_Tween_tween_completed(object, key) -> void:
	if jam_ludis_tween:
		if globala.malfacileco == 1:
			$avertkoloro.visible = false
			$ofteco.wait_time = 1.5
			novvorta_rapideco = 300
		elif globala.malfacileco == 2:
			$avertkoloro.visible = false
			$ofteco.wait_time = 0.8
			novvorta_rapideco = 375
		else:
			$avertkoloro.visible = false
			$ofteco.wait_time = 1.75
			novvorta_rapideco = 250
			
	else:
		tween_agordoj.invert()
		$Tween.interpolate_property($avertkoloro, "modulate", tween_agordoj[0], tween_agordoj[1], 1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT, 0)
		$Tween.start()
		jam_ludis_tween = true

func _on_pauzo_timeout():
	vorteblas = true

func gxisdatigi_alfabeton():
	alfabeto.clear()
	var alfastring : String
	match globala.lingvo_vorto:
		"eo":
			alfastring = "ABCĈDEFGĜHĤIJĴKLMNOPQRSŜTUŬVWXYZ "
		"de":
			alfastring = "AÄBCDEFGHIJKLMNOÖPQRSẞẞẞẞTUÜVWXYZ "	
		"fr":
			alfastring = "AÀÂBCÇDEÉÈÊËFGHIÎÏJKLMNOÔPQRSTUÙÛVWXYZ "
		"ru":
			alfastring = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ "
		"es":
			alfastring = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ "
		"en":
			alfastring = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "
		"sk":
			alfastring = "ABCČDEFGHIJKLMNOPQRSŠTUVWXYZŽ "
		"it":
			alfastring = "ABCDEFGHIJKLMNOPQRSTUVYZ "
		_:
			print("Neniu taŭga alfabeto trovita, do ni donas bazan version")
			alfastring = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "
			
	for c in alfastring:
			alfabeto.append(c)
			
	globala.lingva_alfabeto = alfabeto.duplicate()
	print(globala.lingva_alfabeto)
