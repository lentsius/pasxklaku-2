extends Control

var agord_dosiero = "user://agordoj.cfg"
var ready_finis : bool = false

func _ready():
	#agordoj!
	var tutekrana = globala.sxargxi_agordon("agordoj", "tutekrana", true)
	OS.window_fullscreen = tutekrana
	$vbox/tutekrana.pressed = tutekrana
	
	#unua sxargo de agordo de grizeco
	$vbox/griza.pressed = globala.sxargxi_agordon("agordoj", "griza", false)
	globala.griza = globala.sxargxi_agordon("agordoj", "griza", false)
	
	#helpaj regiloj agordo unua sxargo
	var regiloj : bool = globala.sxargxi_agordon("agordoj", "helpaj_regiloj", false)
	$vbox/regiloj.pressed = regiloj
	globala.helpaj_regiloj = regiloj
	
	#helpaj tremoj agordo unua sxargo
	var tremoj : bool = globala.sxargxi_agordon("agordoj", "tremoj", false)
	$vbox/tremoj.pressed = tremoj
	globala.tremoj = tremoj
	
	#helpaj efektokvanto agordo unua sxargo
	var efektokvanto : float = globala.sxargxi_agordon("agordoj", "efektokvanto", 1.0)
	$vbox/muzikujo2/kvanto.value = efektokvanto
	$vbox/muzikujo2/Videbleco.modulate.a = efektokvanto
	$vbox/muzikujo2/kvanto_teksto.text = str(efektokvanto)
	globala.efektokvanto = efektokvanto
	
	#ui kvanto agordo
	var ui_kvanto : float = globala.sxargxi_agordon("agordoj", "ui_kvanto", 0.33)
	$vbox/uzantinterfaco/ui_kvanto.value = ui_kvanto
	$vbox/uzantinterfaco/kvanto_teksto.text = str(ui_kvanto)
	$vbox/uzantinterfaco/Videbleco.modulate.a = ui_kvanto
	globala.ui_kvanto = ui_kvanto
	
	
	###	VERŜAJNECO
	#gxustkvanto agordo
	var gxustkvanto : float = globala.sxargxi_agordon("agordoj", "gxustkvanto", 0.33)
	$vbox/gxustujo/kvanto.value = gxustkvanto
	$vbox/gxustujo/teksto.text = str(gxustkvanto * 100) + "%"
	globala.gxustsxanco = gxustkvanto
	
	#malgxustkvanto agordo
	var malgxustkvanto : float = globala.sxargxi_agordon("agordoj", "malgxustkvanto", 0.33)
	$vbox/malgxustujo/kvanto.value = malgxustkvanto
	$vbox/malgxustujo/teksto.text = str(malgxustkvanto * 100) + "%"
	globala.malgxustsxanco = malgxustkvanto
	
	#volapukgxustkvanto agordo
	var volapukkvanto : float = globala.sxargxi_agordon("agordoj", "volapukkvanto", 0.33)
	$vbox/volapukujo/kvanto.value = volapukkvanto
	$vbox/volapukujo/teksto.text = str(volapukkvanto * 100) + "%"
	globala.volapuksxanco = volapukkvanto
	
	ready_finis = true
	
func _on_tutekrana_pressed():
	OS.window_fullscreen = $vbox/tutekrana.pressed
	ProjectSettings.set_setting("display/window/size/fullscreen", $vbox/tutekrana.pressed)
	var err = ProjectSettings.save()
	if err:
		print("Problemo estis trovita kaj la agordo por tutekrana bildo ne konservigxis. Bonvolu kontakti subtenon.")
	else:
		print("Sukcese konservis la tutekranan opcion")
	globala.konservi_agordon("agordoj", "tutekrana", $vbox/tutekrana.pressed)


func _on_griza_toggled(button_pressed):
	globala.griza = button_pressed
	globala.konservi_agordon("agordoj", "griza", button_pressed)


func _on_Montriloj_toggled(button_pressed):
	globala.helpaj_regiloj = button_pressed
	globala.konservi_agordon("agordoj", "helpaj_regiloj", button_pressed)


func _on_tremoj_toggled(button_pressed):
	globala.tremoj = button_pressed
	globala.konservi_agordon("agordoj", "tremoj", button_pressed)

func _on_kvanto_value_changed(value):
	$vbox/muzikujo2/kvanto_teksto.text = str(value)
	$vbox/muzikujo2/Videbleco.modulate.a = value
	globala.konservi_agordon("agordoj", "efektokvanto", value)
	globala.efektokvanto = value


func _on_ui_kvanto_value_changed(value):
	$vbox/uzantinterfaco/kvanto_teksto.text = str(value)
	globala.konservi_agordon("agordoj", "ui_kvanto", value)
	globala.ui_kvanto = value
	$vbox/uzantinterfaco/Videbleco.modulate.a = value


#versxajnecojn
func _on_gxustkvanto_value_changed(value):
	kontroli_versxajnecon(value, 0)


func _on_malgxustkvanto_value_changed(value):
	kontroli_versxajnecon(value, 1)


func _on_volapukkvanto_value_changed(value):
	kontroli_versxajnecon(value, 2)


func kontroli_versxajnecon(nova : float, kiu : int) -> float:
	var fina_versxajneco : float
	
	var gxust : float = $vbox/gxustujo/kvanto.value
	var malgxust : float = $vbox/malgxustujo/kvanto.value
	var volapuk : float = $vbox/volapukujo/kvanto.value
	
	var diferenco = 1.0 - (gxust + malgxust + volapuk)
	if ready_finis:
		match kiu:
			0:
				malgxust += diferenco
			1:
				gxust += diferenco
			2:
				gxust += diferenco / 2
				malgxust += diferenco / 2
				
		#konservas valorojn
		globala.konservi_agordon("agordoj", "gxustkvanto", gxust)
		globala.konservi_agordon("agordoj", "malgxustkvanto", malgxust)
		globala.konservi_agordon("agordoj", "volapukkvanto", volapuk)
		
		#donas al la globala skripto
		globala.gxustsxanco = gxust
		globala.malgxustsxanco = malgxust
		globala.volapuksxanco = volapuk
		
	#revenante ni donas novajn valorojn		
	$vbox/gxustujo/kvanto.value = gxust
	$vbox/malgxustujo/kvanto.value = malgxust
	$vbox/volapukujo/kvanto.value = volapuk
	
	#donas la numerojn
	$vbox/gxustujo/teksto.text = str(abs(gxust) * 100) + "%"
	$vbox/malgxustujo/teksto.text = str(abs(malgxust) * 100) + "%"
	$vbox/volapukujo/teksto.text = str(abs(volapuk) * 100) + "%"
	
	
	return fina_versxajneco


func _on_restarti_versxajnecon_pressed():
	# restarti vaolorojn al samaj
	$vbox/gxustujo/kvanto.value = 0.33
	$vbox/malgxustujo/kvanto.value = 0.33
	$vbox/volapukujo/kvanto.value = 0.33
