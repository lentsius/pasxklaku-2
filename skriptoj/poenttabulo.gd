extends ColorRect

var ludantoj = []
var poentoj = []

var leg_ludantoj = []
var leg_poentoj = []

var loko = 0

var entabuligxas = 0

var venkpoentoj = 0

var skribilo

var venkanto

signal entabuligxis

var dauxrigalitersxangxo = false
var sxangxi = false

var unuaklako = true

func _ready() -> void:
	skribilo = preload('res://skriptoj/skribilo.gd').new()
	skribilo.gxisdatigi_alfabeton()
	poentoj = $loko/h_skatolo/poentoj.get_children()
	ludantoj = $loko/h_skatolo/ludantoj.get_children()	
	plenigxi()
	vortigxi()

func gxisdatigxi() -> void:
	nuligxi()
	plenigxi()
	vortigxi()

#warning-ignore:unused_argument
func _process(delta) -> void:
	if entabuligxas:
		var letera_sxangxo = false
		var kursora_sxangxo = false	
#		if venkanto == 1:
		if Input.is_action_just_pressed("ui_up"):
			if unuaklako:
				unuaklaku()
				
			$long_klako.start()
			skribilo.supresube(-1)
			letera_sxangxo = true			
		if Input.is_action_just_pressed("ui_down"):
			if unuaklako:
				unuaklaku()
				
			$long_klako.start()			
			skribilo.supresube(1)
			letera_sxangxo = true
		if Input.is_action_just_pressed("ui_left"):
			if unuaklako:
				unuaklaku()
			skribilo.dekstremaldekstre(-1)
			kursora_sxangxo = true
		if Input.is_action_just_pressed("ui_right"):
			if unuaklako:
				unuaklaku()
			skribilo.dekstremaldekstre(1)
			kursora_sxangxo = true
			
		if Input.is_action_just_released("ui_up") or Input.is_action_just_released("ui_down"):
			$long_klako.stop()
			$sxangxtempo.stop()
			dauxrigalitersxangxo = false
				
				
		if Input.is_action_pressed("ui_up"):
			if dauxrigalitersxangxo == true:
				if sxangxi == true:
					skribilo.supresube(-1)
					letera_sxangxo = true
					sxangxi = false

		if Input.is_action_pressed("ui_down"):
			if dauxrigalitersxangxo == true:
				if sxangxi == true:
					skribilo.supresube(1)
					letera_sxangxo = true
					sxangxi = false

				
		if Input.is_action_just_pressed("nuligi"):
			skribilo.nuligxi()
			letera_sxangxo = true
			
		if Input.is_action_just_pressed("forigi_lastan"):
			if unuaklako:
				unuaklaku()
				letera_sxangxo = true
			else:
				skribilo.forigi_lastan()
				letera_sxangxo = true			
				
		if Input.is_action_just_pressed("ui_cancel") or Input.is_action_just_pressed("select") or Input.is_action_just_pressed("select_l2"):
			if ludantoj[loko].text == TranslationServer.translate("VIA NOMO") or ludantoj[loko].text == "" or ludantoj[loko].text == " ":
				if get_parent().get_parent().name == "fino":
					get_parent().get_parent().nomaverto()
					print("BNVL ENMETU VIAN NOMON")
				else:
					print("BNVL ENMETU VIAN NOMON, NUR TESTAS MI")
			else:	
				ludantoj[loko].modulate = Color(1,1,1)
				poentoj[loko].modulate = Color(1,1,1)				
				entabuligxas = 0
				konservi_tabulon()
				emit_signal("entabuligxis")
				print("SIGNALO")
				$loko/klaku.visible = false
			
		if letera_sxangxo:
			ludantoj[loko].text = skribilo.teksto
		if kursora_sxangxo:
			pass

func unuaklaku() -> void:
	skribilo.nuligxi()
	unuaklako = false

func plenigxi() -> void:
	var dosiero = File.new()
	dosiero.open("user://lingvoj/" + globala.lingvo_vorto + "/tabulo.agordo", File.READ)
	while !dosiero.eof_reached():
		leg_ludantoj.append(str(dosiero.get_line()))
		leg_poentoj.append(int(dosiero.get_line()))
	print(leg_ludantoj.size())
	if leg_ludantoj.size() > 9:
		leg_ludantoj.resize(10)
	if leg_poentoj.size() > 9:
		leg_poentoj.resize(10)
	dosiero.close()

func plenigxi_per_lingvo(lingvo) -> void:
	var dosiero = File.new()
	dosiero.open(globala.lingva_dosierujo + lingvo +"/tabulo.agordo", File.READ)
	while !dosiero.eof_reached():
		leg_ludantoj.append(str(dosiero.get_line()))
		leg_poentoj.append(int(dosiero.get_line()))
	print(leg_ludantoj.size())
	if leg_ludantoj.size() > 9:
		leg_ludantoj.resize(10)
	if leg_poentoj.size() > 9:
		leg_poentoj.resize(10)
	dosiero.close()	

func nuligxi() -> void:
	leg_ludantoj = []
	leg_poentoj = []
	for i in ludantoj:
		i.text = ""
	for i in poentoj:
		i.text = "" 

func vortigxi(lingve : bool = true) -> void:
	if lingve:
		$loko/lingvo.text = globala.lingvo_vorto
	var x = 0
	var longeco = leg_ludantoj.size()
#	for i in leg_ludantoj:
#		ludantoj[leg_ludantoj.find(i)].text = str(i)
#	for i in leg_poentoj:
#		poentoj[leg_poentoj.find(i)].text = str(i)
	while x < longeco:
		ludantoj[x].text = str(leg_ludantoj[x])
		poentoj[x].text = str(leg_poentoj[x])
		x = x + 1
	
	print(leg_ludantoj)
	print(leg_poentoj)

func entabuligx_kontrolo() -> int:
	venkanto = globala.venkis
	var ludantpoentoj
	loko = 0
	if venkanto == 1:
		venkpoentoj = globala.l1_poentoj
		ludantpoentoj = globala.l1_poentoj
		for i in leg_poentoj:
			if ludantpoentoj < i:
				loko += 1
	else:
		venkpoentoj = globala.l2_poentoj
		ludantpoentoj = globala.l2_poentoj
		for i in leg_poentoj:
			if ludantpoentoj < i:
				loko += 1
	print(loko)
	
	if loko < 9:
		entabuligxas = 1
		$loko/klaku.visible = true
		ludantoj[loko].text = skribilo.teksto
		enpoentabuligxi(loko)
		
	return(loko)
	
func testo() -> void:
	entabuligxas = 1
	$loko/klaku.visible = true
	ludantoj[0].text = skribilo.teksto
	enpoentabuligxi(0)
	
func enpoentabuligxi(vicloko) -> void:

	leg_ludantoj.insert(vicloko, "")
	ludantoj[vicloko].modulate = Color(1,0.5,0)
	
	leg_poentoj.insert(vicloko, venkpoentoj)
	poentoj[vicloko].modulate = Color(1,0.5,0)
	
	if leg_ludantoj.size() > 9:
		leg_ludantoj.pop_back()
	if leg_poentoj.size() > 9:	
		leg_poentoj.pop_back()
	vortigxi()
	ludantoj[loko].text = skribilo.teksto
	
func konservi_tabulon() -> void:
	
	var dosiero = File.new()
	dosiero.open("user://lingvoj/" + globala.lingvo_vorto + "/tabulo.agordo", File.WRITE)
	
	for i in ludantoj:
		var numero = ludantoj.find(i)
		dosiero.store_line(ludantoj[numero].text)
		dosiero.store_line(poentoj[numero].text)
	dosiero.close()
	print("poenttabulo koservite!")

func _on_long_klako_timeout() -> void:
	dauxrigalitersxangxo = true
	$sxangxtempo.start()

func _on_sxangxtempo_timeout() -> void:
	sxangxi = true

func _on_tabulojn_forvisxita(lingvo) -> void:
	$loko/lingvo.text = lingvo
	nuligxi()
	plenigxi_per_lingvo(lingvo)
	vortigxi(false)
