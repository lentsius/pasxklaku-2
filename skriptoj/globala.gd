extends Node

#regulaj
var sxlositaj_kategorioj : Array = ["bestoj", "fruktoj", "korpopartoj", "mikso", "vestajxoj"]

#dosiervojoj
const agord_dosiero = "user://agordoj.cfg"
const lingva_dosierujo : = "user://lingvoj/"
const kolora_finajxo : = ".koloro"
const vortara_finajxo : = ".vortaro"
const tabula_finajxo : = ".tabulo"

#ludo-informoj
var gxust_vort_listo : Array = []
var malgxust_vort_listo : Array = []
var volapukajxnombro : int = 0

var ludanto_1_statistikoj : Dictionary = {"frue" : 0 ,"bone" : 0, "mojose" : 0, "neklakite" : 0, "eraroj" : 0}
var ludanto_1_divenitaj : Array = []
var ludanto_1_nedivenitaj : Array = []

var ludanto_2_statistikoj : Dictionary = {"frue" : 0 ,"bone" : 0, "mojose" : 0, "neklakite" : 0, "eraroj" : 0}
var ludanto_2_divenitaj : Array = []
var ludanto_2_nedivenitaj : Array = []

#lingvoj
var lingvoj : = []

var rapideco = 1
var l1_poentoj = 0
var l2_poentoj = 0

var venkis = 0

var jam_ludis = false

var ludo = null
var gui = null
var unue : bool = true
# malfacilecoj estas
# 0 - ordinara, 1 - kosxmaro, 2 - krysxtofa (versxajne oni renomos gxin poste)

var malfacileco = 0
var malfacileco_vorto = "ordinara"

#	lingvoj estas auxtomate prenataj de la dosierujo por lingvoj

var lingvo = 0
var lingvo_vorto = "eo"
var kategorio : String = "mikso"
var lingvo_koloro : Color = Color(00.2,0.2,0.2,1)
var lingva_alfabeto : Array
var redaktata_lingvo : String

#fina bazkoloro
var finbazkoloro

### AGORDOJ
var partikloj = true
var griza : bool = true
var helpaj_regiloj : bool = false
var tremoj : bool = false
var efektokvanto : float = 1.0
var ui_kvanto : float = 1.0
var sxangxi_koloron : bool = false

#versxajneco de vortspecoj
var gxustsxanco : = 0.33
var malgxustsxanco : = 0.33
var volapuksxanco : = 0.33

var frupoentoj : = 50
var bonpoentoj : = 150
var mojospoentoj : = 400

#obligilo por sinsekvaj poentoj
var obligilo : = 175


#altecoj de la linioj por kontroli kie la vortoj estas
var frualteco : float 
var bonalteco : float
var mojosalteco : float

	
func kontroli_sxlositajn(kiun : String) -> bool:
	#la aro kiu havos la kategoriojn por testi
	var testaro : Array = []
	
	var cxu : = false
	
	for i in sxlositaj_kategorioj:
#		testaro.append(TranslationServer.translate(i.to_upper())) <---- ne bezonatas
		testaro.append(i)

	#se nia listo enhavas la kategorion signifas ke ĝi estas protektita
	if testaro.has(kiun):
		cxu = true
	
	return cxu
	
func nuligi_vorton():
	ludo.vorto = null
	
	
func nuligi_poentojn(l):
	if l == 1:
		l1_poentoj = 0
	else:
		l2_poentoj = 0
		
		
func resxalti_ludstaton():
	l1_poentoj = 0
	l2_poentoj = 0
	venkis = 0
	
	
func divenite(ludanto, modo):
	if ludanto == 1:
		match modo:
			"frue":
				ludanto_1_statistikoj.frue += 1
				l1_poentoj += frupoentoj
				gui.ll1_frue()
			"bone":
				ludanto_1_statistikoj.bone += 1
				l1_poentoj += bonpoentoj
				gui.ll1_bone()
			"mojose":
				ludanto_1_statistikoj.mojose += 1
				l1_poentoj += mojospoentoj + (ludo.l1_kombo * obligilo)
				gui.ll1_mojose()
				
	if ludanto == 2:
		match modo:
			"frue":
				ludanto_2_statistikoj.frue += 1
				l2_poentoj += frupoentoj
				gui.ll2_frue()
			"bone":
				ludanto_2_statistikoj.bone += 1
				l2_poentoj += bonpoentoj
				gui.ll2_bone()
			"mojose":
				ludanto_2_statistikoj.mojose += 1
				l2_poentoj += mojospoentoj + (ludo.l2_kombo * obligilo)
				gui.ll2_mojose()
				
				
func mistrafis(ludanto : int):
	gui.mistrafis(ludanto)
	gui.gxisdatigi_kombon(ludanto, 0)
	if ludanto == 1:
		ludanto_1_statistikoj.eraroj += 1
		print("plialtigis eraropoentojn por ludanto 1")
		ludo.l1_kombo = 0
	else:
		ludanto_2_statistikoj.eraroj += 1
		print("plialtigis eraropoentojn por ludanto 2")
		ludo.l2_kombo = 0
		
		
static func copy_directory(path_original : String, path_destination : String) -> void:
	print("Copying directory: " + path_original)
	var dir = Directory.new()
	if dir.open(path_original) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if not file_name in [".", ".."]:
				if dir.current_is_dir():
					copy_directory(path_original + "\\" + file_name, path_destination + "/" + file_name)
				else:
					print("Copying file: " + path_original + "\\" + file_name)
					dir.make_dir_recursive(path_destination)
					dir.copy(path_original + "\\" + file_name, path_destination + "/" + file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
		
		
func konservi_agordon(sekcio, afero, enhavo):
	var dosiero = ConfigFile.new()
	dosiero.load(agord_dosiero)
	dosiero.set_value(sekcio, afero, enhavo)
	dosiero.save(agord_dosiero)
	
	
func sxargxi_agordon(sekcio, afero, cxiukaza):
	var dosiero = ConfigFile.new()
	dosiero.load(agord_dosiero)
	var verdikto = dosiero.get_value(sekcio, afero, cxiukaza)
	
	return verdikto
		
		
func restarti_statistikojn() -> void:
	#gxeneralaj 
	volapukajxnombro = 0
	
	# ludanto 1
	ludanto_1_statistikoj.frue = 0
	ludanto_1_statistikoj.bone = 0
	ludanto_1_statistikoj.mojose = 0
	ludanto_1_statistikoj.neklakite = 0
	ludanto_1_statistikoj.eraroj = 0
	ludanto_1_divenitaj.clear()
	ludanto_1_nedivenitaj.clear()
	
	
	# ludanto 2
	ludanto_2_statistikoj.frue = 0
	ludanto_2_statistikoj.bone = 0
	ludanto_2_statistikoj.mojose = 0
	ludanto_2_statistikoj.neklakite = 0
	ludanto_2_statistikoj.eraroj = 0
	ludanto_2_divenitaj.clear()
	ludanto_2_nedivenitaj.clear()
	
func forigi_vortliston() -> void:
	gxust_vort_listo.clear()
	malgxust_vort_listo.clear()
	
func preni_kategoriojn(alia_lingvo : String = "") -> Array:
	#func gxisdatigi_kategoriojn(language_id : int = 0, alia_lingvo : bool = false):
	var kategorioj : Array = []
	var dosierujo : Directory = Directory.new()
	#warning-ignore:return_value_discarded
	if alia_lingvo != "":
		dosierujo.open(lingva_dosierujo + alia_lingvo + "/")
	else:
		dosierujo.open(lingva_dosierujo + lingvo_vorto + "/")
	#warning-ignore:return_value_discarded
	dosierujo.list_dir_begin(true,true)
	var kategorio = dosierujo.get_next()
	while kategorio != "":
#		if kategorio != "" and !kategorio.ends_with(".txt") and !kategorio.ends_with(".agordo") and !kategorio.ends_with(".png") and !kategorio.ends_with(".import"):
		if dosierujo.current_is_dir():
			kategorioj.append(kategorio)
		kategorio = dosierujo.get_next()
	
	dosierujo.list_dir_end()

	print("La kategorioj estas:", kategorioj)
	
	return kategorioj
	
func preni_flageton() -> Texture:
	var flago : Texture = load(lingva_dosierujo +  lingvo_vorto + "/flageto.png")
	print(flago)
	
	return flago
