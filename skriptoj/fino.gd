extends Control

var l1_pretas = false
var l2_pretas = false
var pauzite = false

var funkcio = false

var entabuligxas = false

func _ready():
#	$ColorRect/ColorRect2.color = globala.lingvo_koloro
	$entajpfono.visible = false
	$sonoj/aplaudo.play()
	kontroli_poentojn()
	funkcio = true
	

func _process(delta):
	if funkcio:
		#sistemaj butonoj, helpaj precipe por menuoj
		if Input.is_action_just_pressed("ui_select"):
			if entabuligxas:
				pass
				
		
func kontroli_poentojn():
	var tr = TranslationServer
	if globala.l1_poentoj > globala.l2_poentoj:
		$Statistikoj/L1/Sukceso/L1_nomo.modulate = Color("ffdd00")
		$Statistikoj/L1/Sukceso/Fono.modulate = Color("ffdd00")
		globala.venkis = 1
		$tabulo/H/gajnanto.text = tr.translate("L1")
		$tabulo/H/poentoj.text = str(globala.l1_poentoj)
		
		if $tabulo/poenttabulo2.entabuligx_kontrolo() < 9:
			$tabulo/priskribo.text = tr.translate("ENPOENTTABULIGXIS")
			entabuligxas = true
			$entajpfono.visible = true
		else:
			fari_preskau()
			
			
	if globala.l1_poentoj < globala.l2_poentoj :
		globala.venkis = 2
		$Statistikoj/L2/Sukceso/L2_nomo.modulate = Color("ffdd00")
		$Statistikoj/L2/Sukceso/Fono.modulate = Color("ffdd00")
		$tabulo/H/gajnanto.text = tr.translate("L2")
		$tabulo/H/poentoj.text = str(globala.l2_poentoj)
		
		if $tabulo/poenttabulo2.entabuligx_kontrolo() < 9:
			$tabulo/priskribo.text = tr.translate("ENPOENTTABULIGXIS")
			entabuligxas = true
			$entajpfono.visible = true
		else:
			fari_preskau()
	
	if globala.l1_poentoj == globala.l2_poentoj:
		globala.venkis = 3
		$tabulo/H/gajnanto.text = tr.translate("SAMPOENTA")
		$tabulo/H/poentoj.text = str(globala.l1_poentoj)
#		$sonoj/l2_venkas.play()
		if $tabulo/poenttabulo2.entabuligx_kontrolo() < 9:
			$tabulo/priskribo.text = tr.translate("AMBAUX_ENTABULIGXIS")
			entabuligxas = true
			$entajpfono.visible = true
		else:
			$tabulo/priskribo.text = tr.translate("GRATULON")
			$fin_menuo/V/denove.grab_focus()
			
			
func resxalti_ludon():
	globala.restarti_statistikojn()
	globala.resxalti_ludstaton()
	get_tree().change_scene('res://scenoj/gustamalgusta.tscn')	


func _on_fermi_pressed():
	get_tree().quit()


func _on_menuo_pressed():
	globala.restarti_statistikojn()
	globala.resxalti_ludstaton()
	get_tree().paused = false
	get_tree().change_scene('res://scenoj/menuo.tscn')


func _on_denove_pressed():
	resxalti_ludon()


func _on_poenttabulo_entabuligxis():
	entabuligxo_finigxis()
	
	
func _on_poenttabulo2_entabuligxis():
	entabuligxo_finigxis()
	

func entabuligxo_finigxis() -> void:
	entabuligxas = false
	$entajpfono.visible = false
	$fin_menuo/V/denove.grab_focus()
	$Statistikoj.komenci_tvinon()
	
func fari_preskau() -> void:
	$tabulo/priskribo.text = TranslationServer.translate("PRESKAUX")
	$fin_menuo/V/denove.grab_focus()
	$Statistikoj.komenci_tvinon()
	
func nomaverto():
	$nomaverto/AnimationPlayer.play("nomaverto")

func _on_VORTOJ_pressed():
	$VortListo.aperi()
	
	
func reveni_al() -> void:
	$fin_menuo/V/denove.grab_focus()
