extends Control


var orig_poz : Vector2


func _ready():
	$vortujo/kategorioj/Protekti.modulate.a = 0
	orig_poz = $Menufono.rect_position
	$gxeneralujo.visible = false
	$vortujo.visible = false
	$tapisxujo.visible = false
	$montriloj.visible = false		
	$tabulojn.visible = false
	$muzikujo.visible = false
	self.visible = false	
	
	
func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel") and visible:
		eliru()


func _on_reen_pressed():
	eliru()
	
	
func eliru() -> void:
	$Sxovo.play()
	$vortujo.visible = false
	$muzikujo.visible = false
	$tapisxujo.visible = false
	$montriloj.visible = false
	$tabulojn.visible = false
	$gxeneralujo.visible = false
	
	$Tvino.interpolate_property($AgordFono, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($Menufono, 'rect_position', orig_poz, Vector2(orig_poz.x - $Menufono.rect_size.x, orig_poz.y), 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	get_parent().get_child(2).visible = true
	get_parent().agordoj = false
	get_parent().unua_butono.grab_focus()	
	get_parent().centrigi_emblemon()
	
	yield($Tvino, 'tween_completed')
	
	self.visible = false
	
	
func eniru() -> void:
	$Sxovo.play()
	$Tvino.interpolate_property($Menufono, 'rect_position',Vector2(orig_poz.x - $Menufono.rect_size.x, orig_poz.y), orig_poz, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($AgordFono, 'modulate', Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	aktualigi_rekordtabulon()
	get_parent().get_child(2).visible = false
	visible = true
	update()
	get_parent().agordoj = true
	$Menufono/HBoxContainer/VBoxContainer/ekrano.grab_focus()


func aktualigi_rekordtabulon() -> void:
	$tabulojn/HBoxContainer/VBoxContainer/CenterContainer/LudantOpcio2.select(globala.lingvo)
	$tabulojn/HBoxContainer/poenttabulo.gxisdatigxi()


func _on_danctapiso_pressed():
	kasxi_krom($tapisxujo, $montriloj)


func _on_vortojn_pressed():
	#ni refresxigu ĝin
	$vortujo.alia_lingvo = globala.lingvo_vorto
	globala.redaktata_lingvo = globala.lingvo_vorto
	$vortujo.update(true)
	$vortujo.gxisdatigi_kategoriojn()
	if $vortujo/kategorioj/kategorio.get_item_count() > 0:
		$vortujo/kategorioj/kategorio.select(1)
	kasxi_krom($vortujo)
	

func _on_Rekordtabulojn_pressed():
	kasxi_krom($tabulojn)

	
func _on_ekrano_pressed():
	kasxi_krom($gxeneralujo)


func _on_muziko_pressed():
	kasxi_krom($muzikujo)


func kasxi_krom(unu = null, du = null):
	$gxeneralujo.visible = false
	$tabulojn.visible = false
	$vortujo.visible = false
	$tapisxujo.visible = false
	$montriloj.visible = false
	$muzikujo.visible = false
	
	if unu == null:
		return
		
	unu.visible = true
	if du == null:
		return
		
	du.visible = true
