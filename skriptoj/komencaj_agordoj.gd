extends Node


var agord_dosiero = "user://agordoj.cfg"


func _ready():
	var dosiero = ConfigFile.new()
	var eraro = dosiero.load(agord_dosiero)
	
	#se la dosiero ne ekzistas ni kreu gxin
	if eraro:
		print("ERARO MI KREAS NOVAN BAZAN DOSIERON")
		dosiero.set_value("agordoj", "tutekrana", true)
		dosiero.set_value("agordoj", "griza", false)
		dosiero.set_value("agordoj", "partiklaj_efektoj", true)		
		dosiero.set_value("agordoj", "gxenerala_lauteco", 100)
		dosiero.set_value("agordoj", "muzik_lauteco", 100)
		dosiero.set_value("agordoj", "efekt_lauteco", 100)
		dosiero.save(agord_dosiero)
		
		
	#se gxi ja jam ekzistas ni uzu la agordojn
	else:
		var gxenerala_lauteco = dosiero.get_value("agordoj", "gxenerala_lauteco", 100)
		var muzik_lauteco = dosiero.get_value("agordoj", "muzik_lauteco", 100)
		var efekt_lauteco = dosiero.get_value("agordoj", "efekt_lauteco", 100)
		var lautecoj = [gxenerala_lauteco, muzik_lauteco, efekt_lauteco]
		for lauteco in lautecoj:
			if lauteco == 0:
				AudioServer.set_bus_mute(lautecoj.find(lauteco), true)
			else:
				AudioServer.set_bus_mute(lautecoj.find(lauteco), false)
				var db = konverti_db(gxenerala_lauteco)
				AudioServer.set_bus_volume_db(lautecoj.find(lauteco), db)
		
		globala.partikloj = dosiero.get_value("agordoj", "partiklaj_efektoj", true)
		
		
func konverti_db(val):
	var db = val - 100
	if db > 0:
		db = db / 8
	
	return db
	
	
