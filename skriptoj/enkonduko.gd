extends Control

var agord_dosiero = 'user://agordoj.cfg'
var lingvoj : = []
var klakis : = false

func _ready():
	$Loading.modulate.a = 0
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	var tutekrana = globala.sxargxi_agordon("agordoj", "tutekrana", true)
	if tutekrana:
		OS.window_borderless = true
		OS.window_fullscreen = tutekrana
	
	$unua/animacioj.play("unua")
			
func fino():
	$Tvino.interpolate_property($Loading, "modulate:a", 0, 1, 1.0, Tween.TRANS_SINE, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	yield($Tvino, 'tween_completed')
	kontroli()
	var eraro = get_tree().change_scene("res://scenoj/menuo.tscn")
	print(eraro)

func _process(delta):
	kontroli_klakon()

func kontroli_klakon():
	if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_select"):
		if klakis:
			return
		$unua/animacioj.stop(false)
		klakis = true
		fino()

	
func kontroli() -> void:
	# klasoj
	var dosierujo = Directory.new()
	var dosiero = File.new()

	var debug : bool = ProjectSettings.get_setting("application/config/debugging")
	var originalaj_lingvoj : String = OS.get_executable_path()
	var finajxo : int = originalaj_lingvoj.find_last("\\")
	if finajxo == -1:
		finajxo = originalaj_lingvoj.find_last("/")
	if finajxo != -1:
		originalaj_lingvoj.erase(finajxo + 1, originalaj_lingvoj.length() - finajxo)
		originalaj_lingvoj += "lingvoj"
		
	print(originalaj_lingvoj)
#
#	#kontroli ĉu ekzistas la dosierujo en la uzant dosierujo
	var ekzistas : bool = dosierujo.dir_exists(globala.lingva_dosierujo)
	
	if ekzistas:
		if ProjectSettings.get_setting("application/config/version") != globala.sxargxi_agordon("ludo", "versio", 0):
			globala.konservi_agordon("ludo", "versio", ProjectSettings.get_setting("application/config/version"))
			print("nova versio de la ludo do ni kopias denove!")
			if debug:
				globala.copy_directory("res://vortoj/", "user://lingvoj/")
				OS.alert("you're running a debug build!", "Debug warning")
			else:
				globala.copy_directory(originalaj_lingvoj, "user://lingvoj/")
				
		else:
			print("sama versio kaj dosierujo jam estas, do ni kopias nenion")
	else:
		print("uzant dosierujo ne ekzistas, oni kopias freŝan version")
		if debug:
			globala.copy_directory("res://vortoj/", "user://lingvoj/")
		else:
			globala.copy_directory(originalaj_lingvoj, "user://lingvoj/")
		
		
	#kon9troli linvojn
	kontroli_lingvojn(dosierujo)
		
#	jen ni kontrolas la poenttabulojn, kaj se ne ekzistas ni kreu!
	for i in lingvoj:
		if !dosiero.file_exists(globala.lingva_dosierujo + i + "/" + "tabulo.agordo"):
			dosiero.open(globala.lingva_dosierujo + i + "/" + "tabulo.agordo", File.WRITE)
			print("kreis la " + str(i) + " poenttabulon")
		else:
			print("tabulo " + str(i) + " jam agorditaj")
			
	
func kontroli_lingvojn(dos : Directory) -> void:
	dos.open(globala.lingva_dosierujo)
	dos.list_dir_begin(true, true)
	var lingvo = dos.get_next()
	
	while lingvo != "":
		print(lingvo)
		if dos.current_is_dir():
			lingvoj.append(lingvo)
		lingvo = dos.get_next()
		
	print("lingvoj estas: ", lingvoj)
	globala.lingvoj = lingvoj.duplicate()
