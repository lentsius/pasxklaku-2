extends Control

var dosiervojo : String = "user://lingvoj/"
var sufikso : String = "/tabulo.agordo"

var cxiuj = []

# tiu cxi variablo tenas la nunan tabullon por forvisxi
var tabuloj = []

# variablo kiu tenas la nomo de la lingvo
var lingvo = "en"

# cxu ciujn?
var cxiujn = false

# signalo kiu estas forsendita kiam iu tabulo estas forvisxita kaj kiam ni volas montri alian lingvon cxe la rekord tabulo
signal forvisxita(lingvo)

func _ready():
	$demando.visible = false
	
	for i in globala.lingvoj:
		$HBoxContainer/VBoxContainer/CenterContainer/LudantOpcio2.add_item(i, -1)
	
func forvisxi_cxiujn():
	for i in range(0, $HBoxContainer/VBoxContainer/CenterContainer/LudantOpcio2.get_item_count()):
		forvisxi($HBoxContainer/VBoxContainer/CenterContainer/LudantOpcio2.get_item_text(i))
		print("forvisxis " + str(i))
	

func forvisxi(forvisxindajxo):
	var dosiero = File.new()
	var eraro = dosiero.open(globala.lingva_dosierujo + forvisxindajxo + sufikso, File.WRITE)
	if eraro:
		print("Rekordtabulo ne ekzistas.")
		return
	dosiero.store_string("")
	dosiero.close()
	emit_signal("forvisxita", lingvo)

func demandi():
	$demando.visible = true
	
###		SIGNALOJ		###

func _on_jes_pressed():
	if cxiujn:
		forvisxi_cxiujn()
	else:
		forvisxi(lingvo)
	$demando.visible = false

func _on_ne_pressed():
	$demando.visible = false

func _on_LudantOpcio2_item_selected(ID):
	lingvo = $HBoxContainer/VBoxContainer/CenterContainer/LudantOpcio2.get_item_text(ID)
	emit_signal("forvisxita", lingvo)

func _on_Forvisxi_pressed():
	cxiujn = false
	demandi()

func _on_ForvisxiCxiujn_pressed():
	cxiujn = true
	demandi()


func _on_ColorRect2_gui_input(event):
	if event is InputEventMouseButton:
		$demando.visible = false
