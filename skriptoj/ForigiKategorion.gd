extends ColorRect


var foriganta : String = ""
var lingvo : String = ""
signal forigis_kategorion


func _ready() -> void:
	visible = false


func aperi() -> void:
	$Tvino.interpolate_property(self, 'modulate:a', 0, 1, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	visible = true


func malaperi() -> void:
	modulate.a = 0
	visible = false
	

func _on_Aldoni_pressed() -> void:
	aperi()


func _on_Reen_pressed():
	malaperi()

	
func _on_Konfirmi_pressed():
	forigi_kategorion()
	malaperi()
	
	
func forigi_kategorion():
	var ujo : Directory = Directory.new()
	var dosiero = File.new()
	
	var katujo : String = globala.lingva_dosierujo + lingvo + "/" + foriganta
	print(katujo)
	var er = ujo.open(katujo)
	print(er)
	print(ujo.get_current_dir())
	ujo.list_dir_begin(true, true)
#
	var dos = ujo.get_next()
##
	while dos != "":
		ujo.remove(ujo.get_current_dir() + "/" + dos)
		print("forigis ", dos)
		dos = ujo.get_next()
#
	ujo.list_dir_end()
	ujo.remove(katujo)
#
	emit_signal("forigis_kategorion")
	

func forigi(kategorion : String) -> void:
	aperi()
	foriganta = kategorion.to_lower()
	$Ekrano/VBox/Nomo.text = kategorion + "?"
