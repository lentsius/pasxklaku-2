extends Control

var malfacilecoj = 1
var malfacileco = 0

var pasinta_lingvo = 0
var lingvoj = 2
var lingvo = 0
export(Color, RGBA) var neaktiva : Color = Color(1,1,1,0.2)
export(Color, RGBA) var aktiva : Color = Color(1,1,1,1)

var poenttabulo = false

var kreditoj = false

var agordoj = false

onready var unua_butono = $menuo/HBoxContainer/VBoxContainer/ludujo/ludi

func _ready():
	randomize()
	$menuo.visible = true
	
	#farbi la fonon laux la lingvo
	if lingvo != globala.lingvo:
		lingvo = globala.lingvo
	gxisdatigi_fonon()
	unua_butono.grab_focus()
	malfacilecoj = $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacilecujo.get_children().size() -1
#	print("menuo komencas funkcii")
	reveno_de_komencekrano()

func _process(delta):
#	if !poenttabulo:
	if $menuo/HBoxContainer/VBoxContainer/ludujo/ludi.has_focus() and Input.is_action_just_pressed("ui_left"):
		gxisdatigi_lingvon(-1)
#		print("klakis")
	if $menuo/HBoxContainer/VBoxContainer/ludujo/ludi.has_focus() and Input.is_action_just_pressed("ui_right"):
		gxisdatigi_lingvon(1)
	
	if $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacileco.has_focus() and Input.is_action_just_pressed("ui_left"):
		gxisdatigi_malfacilecon(-1)
	if $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacileco.has_focus() and Input.is_action_just_pressed("ui_right"):
		gxisdatigi_malfacilecon(1)

		
	if Input.is_action_just_pressed("ui_accept"):
		if !agordoj:
			if $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacileco.has_focus():
				komenci()
			

func gxisdatigi_lingvon(numero):
	if numero < 0:
		if lingvo + numero < 0:
			lingvo = lingvoj
		else:
			pasinta_lingvo = lingvo
			lingvo += numero
		farendajxoj()
		
	else:
		if lingvo + numero > lingvoj:
			lingvo = 0
		else:
			pasinta_lingvo = lingvo
			lingvo += numero
			
		farendajxoj()
	
	
func farendajxoj():
	#unue sxangxu la nomon de la lingvo
	globala.lingvo = lingvo
	globala.lingvo_vorto = $menuo/HBoxContainer/VBoxContainer/ludujo/lingvujo.get_children()[lingvo].name
		
	#ni prenu koloron de la lingvo de la dosierujo
	var kolor_dosiero = File.new()
	var eraro = kolor_dosiero.open("res://vortoj/" + globala.lingvo_vorto + "/lingvo_koloro.txt", 1)
	if eraro == OK:
#		print("malefermis la dosieron! Kaj prenis koloron de: " + globala.lingvo_vorto)
		var nova_koloro : String = kolor_dosiero.get_as_text()
		globala.lingvo_koloro = Color(nova_koloro)
		kolor_dosiero.close()
	else:
#		print("Ne trovis koloron por lingvo: " + globala.lingvo_vorto)
		globala.lingvo_koloro = Color(0.2,0.2,0.2,1)

	for i in $menuo/HBoxContainer/VBoxContainer/ludujo/lingvujo.get_children():
		i.modulate = neaktiva
	$menuo/HBoxContainer/VBoxContainer/ludujo/lingvujo.get_children()[lingvo].modulate = aktiva
	

	gxisdatigi_fonon()

	TranslationServer.set_locale(globala.lingvo_vorto)
	$Lingvo.aktualigi_lingvon()
	lumigxi()
	tradukigxi()
	
	
func gxisdatigi_malfacilecon(numero):
	if numero < 0:
		if malfacileco + numero < 0:
#			print("jam la unua malfacileco")
			malfacileco = malfacilecoj
		else:
			malfacileco += numero
		
	else:
		if malfacileco + numero > malfacilecoj:
#			print("jam la fina malfacileco")
			malfacileco = 0
		else:
			malfacileco += numero
	
	for i in $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacilecujo.get_children():
		i.modulate = neaktiva
	$menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacilecujo.get_children()[malfacileco].modulate = aktiva
	
	globala.malfacileco = malfacileco
	globala.malfacileco_vorto = $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacilecujo.get_children()[malfacileco].name
#	print(globala.malfacileco_vorto)



func _on_fermi_button_down():
	get_tree().quit()


func _on_ludi_button_down():
	komenci()


func gxisdatigi_fonon():
	if globala.sxangxi_koloron:
		$fono/tvino.interpolate_property($fono, "modulate", $fono.get_modulate(), globala.lingvo_koloro, 1, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
		$fono/tvino.start()
	var flago : Texture = load("res://vortoj/" +  globala.lingvo_vorto + "/flago.png")
	var flagujo = $Flago
	flagujo.material.set_shader_param("flago", flago)
#	flagujo.rect_size = OS.get_real_window_size()
#	$fono.modulate = koloroj[lingvo]
#	print("la fonkoloro sxangxigxis")


func tradukigxi():
	pass
	
	
func lumigxi() -> void:
	$Strio/Animacio.play("strio", -1, 5.0)
	$Lingvo/Tvino.interpolate_property($Lumigxo, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Lingvo/Tvino.interpolate_property($Partikloj, 'modulate', Color(1,1,1,1), Color(1,1,1,0.15), 3.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Lingvo/Tvino.start()
	
		
func reveno_de_komencekrano():
	$Emblemo.centrigxi()
	if globala.lingvo_vorto != "eo":
		#se la lingvo ne estas esperanto, nu ni bezonas sxangxojn!!!
		for i in $menuo/HBoxContainer/VBoxContainer/ludujo/lingvujo.get_children():
			i.modulate = neaktiva
		# ni prenu la lingvonumeron de la globala skripto kaj aplikas gxin a lla loka variablo
		lingvo = globala.lingvo
		$menuo/HBoxContainer/VBoxContainer/ludujo/lingvujo.get_children()[lingvo].modulate = aktiva

	if globala.malfacileco != 0:
		for i in $menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacilecujo.get_children():
			i.modulate = neaktiva
		if globala.malfacileco != null:
			malfacileco = globala.malfacileco
		$menuo/HBoxContainer/VBoxContainer/malfacilecujo/malfacilecujo.get_children()[malfacileco].modulate = aktiva
	
	
func _on_kreditoj_pressed():
	kreditoj = true
	$mencioj.eniri()
	
	
#func _input(event):
##	if event is InputEventJoypadButton or event is InputEventAction or event is InputEventMouseButton or event is InputEventMouseMotion:
#	if kreditoj == false:
#		return
#	else:
#		kreditoj = false
#		$Kreditoj.visible = false
	
#	malfermi retumilon kiam la uzanto klakas START aux SPAC BUTONO	
#	if event is InputEventAction:
#		if event.action == "ui_accept" and kreditoj == true:
#			OS.shell_open("http://ikso.net")


func komenci():
	globala.finbazkoloro = globala.lingvo_koloro
	self.set_process(false)
	if !$komencekrano.aktiva:
		$komencekrano.aperi()
		

func _on_agordoj_pressed():
	$Emblemo.maldekstrigxi()
	$agordoj.eniru()
	

func _on_lingvujo_lingvoj_pretas(kiom):
	lingvoj = kiom - 1
	lingvo = 0
	farendajxoj()
	
	
func centrigi_emblemon() -> void:
	$Emblemo.centrigxi()
	

func peti_iri_al_pagxo(kiu : String = "") -> void:
	$Peto.eniri(kiu)
