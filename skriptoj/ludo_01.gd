extends Node2D

var vorto : Array = []
var vorto_l2 : Array = []

var pauzeblas = false

var l1_kombo = 0
var l2_kombo = 0

var l1_vertikale = 0
var l2_vertikale = 0

signal nedivenite(ludanto)
signal diveno(ludanto, tipo)

func _ready():
	globala.ludo = self

func _process(delta):
	ludanto_unu()
	ludanto_du()
	pauzi()

	
func pauzi():
	if Input.is_action_just_pressed("start") or Input.is_action_just_pressed("start_l2"):
		ludu_sonon("klako")		
		if pauzeblas:
			get_tree().paused = true
			$'../pauzo'.visible = true
			$'../pauzo'.pauzite = true
	
	
func ludanto_unu():
	## Ludanto unu klaksonoj
	if Input.is_action_just_pressed("right") or Input.is_action_just_pressed("left") or Input.is_action_just_pressed("down") or Input.is_action_just_pressed("up"):
		ludu_sonon("klako")

	## MALDEKSTREN!!
	if Input.is_action_just_pressed("right") and vorto.size() != 0:
		if !vorto[0].l1_klakis:
			var divenite : Array = vorto[0].klakate(1,1)
			if divenite[0]:
				emit_signal("diveno", 1, divenite[1])
			else:
				emit_signal("nedivenite", 1)

			if divenite[2]:
				l1_kombo += 1
			else:
				l1_kombo = 0
				
			vorto.remove(0)
				
			globala.gui.gxisdatigi_kombon(1, l1_kombo)
			

	## DEKSTREN!!
	if Input.is_action_just_pressed("left") and vorto.size() != 0:
		if !vorto[0].l1_klakis:
			var divenite : Array = vorto[0].klakate(1,2)
			if divenite[0]:
				emit_signal("diveno", 1, divenite[1])
			else:
				emit_signal("nedivenite", 1)	

			if divenite[2]:
				l1_kombo += 1
			else:
				l1_kombo = 0
				
			vorto.remove(0)
				
			globala.gui.gxisdatigi_kombon(1, l1_kombo)


	## VERTIKALE!! IOM DA LOGIKO!!!
	if Input.is_action_just_pressed("down"):
		if vorto.size() == 0:
#			print("senvorta klako")
			return
			
		if l1_vertikale == 0:
			l1_vertikale = 1
			$l1_pasxtempilo.start()
			
	if Input.is_action_just_pressed("up"):
		if vorto.size() == 0:
#			print("senvorta klako")
			return
		
		if l1_vertikale == 0:
			$l1_pasxtempilo.start()
			l1_vertikale = 2

	if Input.is_action_just_released("up") or Input.is_action_just_released("down"):
		l1_vertikale = 0

	if l1_vertikale == 1:
		if Input.is_action_just_pressed("up"):
			vertikala_pasxklako_l1()

	if l1_vertikale == 2:
		if Input.is_action_just_pressed("down"):
			vertikala_pasxklako_l1()

func vertikala_pasxklako_l1():
	if !vorto[0].l1_klakis:
		var divenite : Array = vorto[0].klakate(1,3)
		if divenite[0]:
			emit_signal("diveno", 1, divenite[1])
		else:
			emit_signal("nedivenite", 1)
			
		if divenite[2]:
			l1_kombo += 1
		else:
			l1_kombo = 0
			
		vorto.remove(0)
			
		globala.gui.gxisdatigi_kombon(1, l1_kombo)


func ludanto_du():
	
	## Ludanto unu klaksonoj
	if Input.is_action_just_pressed("right_l2") or Input.is_action_just_pressed("left_l2") or Input.is_action_just_pressed("down_l2") or Input.is_action_just_pressed("up_l2"):
		ludu_sonon("klako")
	
	## MALDEKSTREN!!
	if Input.is_action_just_pressed("right_l2") and vorto_l2.size() != 0:
		if !vorto_l2[0].l2_klakis:
			var divenite : Array = vorto_l2[0].klakate(2,1)
			if divenite[0]:
				emit_signal("diveno", 2, divenite[1])
			else:
				emit_signal("nedivenite", 2)
				
			if divenite[2]:
				l2_kombo += 1
			else:
				l2_kombo = 0
				
			vorto_l2.remove(0)
				
			globala.gui.gxisdatigi_kombon(2, l2_kombo)

	## DEKSTREN!!
	if Input.is_action_just_pressed("left_l2") and vorto_l2.size() != 0:
		if !vorto_l2[0].l2_klakis:
			var divenite : Array = vorto_l2[0].klakate(2,2)
			if divenite[0]:
				emit_signal("diveno", 2, divenite[1])
			else:
				emit_signal("nedivenite", 2)
				
			if divenite[2]:
				l2_kombo += 1
			else:
				l2_kombo = 0
				
			vorto_l2.remove(0)
				
			globala.gui.gxisdatigi_kombon(2, l2_kombo)

	## VERTIKALE!! IOM DA LOGIKO!!!
	if Input.is_action_just_pressed("down_l2"):
		if vorto_l2.size() == 0:
#			print("senvorta klako")
			return

		if l2_vertikale == 0:
			$l2_pasxtempilo.start()
			l2_vertikale = 1
			
	if Input.is_action_just_pressed("up_l2"):
		if vorto_l2.size() == 0:
#			print("senvorta klako")
			return

		if l2_vertikale == 0:
			$l2_pasxtempilo.start()
			l2_vertikale = 2

	if Input.is_action_just_released("up_l2") or Input.is_action_just_released("down_l2"):
		$l2_pasxtempilo.stop()
		l2_vertikale = 0

	if l2_vertikale == 1:
		if Input.is_action_just_pressed("up_l2"):
			vertikala_pasxklako_l2()

	if l2_vertikale == 2:
		if Input.is_action_just_pressed("down_l2"):
			vertikala_pasxklako_l2()

func vertikala_pasxklako_l2():
	if !vorto_l2[0].l2_klakis:
		var divenite : Array = vorto_l2[0].klakate(2,3)
		if divenite[0]:
			emit_signal("diveno", 2, divenite[1])
		else:
			emit_signal("nedivenite", 2)

		if divenite[2]:
			l2_kombo += 1
		else:
			l2_kombo = 0
			
		vorto_l2.remove(0)
			
		globala.gui.gxisdatigi_kombon(2, l2_kombo)

## la fino de ludantklakfunkcioj

func _on_Timer_timeout():
	$pauztempo.stop()
	pauzeblas = true
	
func fino():
	$fin_tempilo.start()

func _on_fin_tempilo_timeout():
	la_fin()
	
func ludu_sonon(sono):
	if sono == "klako":
		$'../sonoj/klako'.play()
	
func _on_l2_pasxtempilo_timeout():
	l2_vertikale = 0

func _on_l1_pasxtempilo_timeout():
	l1_vertikale = 0


#la vorto eniras la ludtablon
func _on_frue_body_entered(body):
	vorto.append(body)
	vorto_l2.append(body)
#
func _on_bone2_body_entered(body):
	body.fino()

func _on_maltrafo_body_entered(body):
	if vorto.has(body):
		vorto.remove(0)
	if vorto_l2.has(body):
		vorto_l2.remove(0)
		
	body.maltrafi()

func _on_punktofino_timeout():
	la_fin()
	
func la_fin():
	globala.jam_ludis = true
	$'../sonoj/muziko'.nunakanto.stop()
	get_tree().change_scene("res://scenoj/fino.tscn")
