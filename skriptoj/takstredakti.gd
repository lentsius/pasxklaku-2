extends Node2D

var mySkribilo

class skribilo:
	
	var alfabeto = []
	var kursoro = 0
	var teksto = "VIA NOMO" # komenca teksto
	
	func _init():
		var alfastring = "ABCĈDEFGĜHĤIJĴKLMNOPQRSŜTUŬVWXYZ "
		for c in alfastring:
			self.alfabeto.append(c)
			
	func supresube(supre):
		# sxangxas la leteron je la kursoro kun la venonta (se supre = 1)
		# aux kun la antauxa (se supre = -1) laux la alfabeto
		var letero = self.teksto.substr(self.kursoro, 1)
		var nova_letero_index = (self.alfabeto.find(letero) + supre) % len(self.alfabeto)
		var nova_letero = self.alfabeto[nova_letero_index]
		
		self.teksto = self.teksto.substr(0, self.kursoro) + \
			nova_letero + \
			self.teksto.substr(self.kursoro+1, len(self.teksto))
		
	func dekstremaldekstre(dekstre):
		# igas antauxen/malantauxen la kursoron
		self.kursoro += dekstre
		# limigas la maksimumon je la longeco de la teksto
		self.kursoro = min(self.kursoro, len(self.teksto)-1)
		# limigas la minimumon je nulo
		self.kursoro = max(self.kursoro, 0)
		
func _ready():
	mySkribilo = skribilo.new()
	$vbc/LabelNomo.text = mySkribilo.teksto
	$vbc/LabelCursor.text = kursorteksto(mySkribilo.kursoro)
	
func kursorteksto(n):
	# simpla videblido de la kursoro
	var txt = ""
	for i in range(n):
		txt += " "
	txt += "^"
	return txt

func _process(delta):
	var letera_sxangxo = false
	var kursora_sxangxo = false
	
	if Input.is_action_just_pressed("ui_up"):
		mySkribilo.supresube(1)
		letera_sxangxo = true
	if Input.is_action_just_pressed("ui_down"):
		mySkribilo.supresube(-1)
		letera_sxangxo = true
	if Input.is_action_just_pressed("ui_right"):
		mySkribilo.dekstremaldekstre(1)
		kursora_sxangxo = true
	if Input.is_action_just_pressed("ui_left"):
		mySkribilo.dekstremaldekstre(-1)
		kursora_sxangxo = true
	
	if letera_sxangxo:
		$vbc/LabelNomo.text = mySkribilo.teksto
	
	if kursora_sxangxo:
		$vbc/LabelCursor.text = kursorteksto(mySkribilo.kursoro)

