extends ColorRect

onready var baza_teksto = preload("res://assets/vorto/GxustaVorto.tscn")

#	kaŝu tuj kaj nur poste ni montru la aferaĉon
func _ready() -> void:
	modulate.a = 0
	visible = false
	
	gxisdati_vortojn()
		
		
#	ĝuste tiel ĉi ni aperu
func aperi() -> void:
	modulate.a = 0
	visible = true
	$Tvino.interpolate_property(self, 'modulate:a', 0, 1, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	
#	kaj malaperu
func malaperi() -> void:
	$Tvino.interpolate_property(self, 'modulate:a', 1, 0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	get_parent().reveni_al()
	
	yield($Tvino, 'tween_completed')
	
	visible = false
	
	
#	ho oni ne forgesu pri tio ke la vortoj devas aperi, ial.
func gxisdati_vortojn() -> void:
	# unue ni forigu eksitantajn
#	for i in $MarginContainer/Ujo/Gxustaj/ScrollContainer/Gxustvortlisto.get_children():
#		i.queue_free()
#	for i in $MarginContainer/Ujo/Gxustaj2/ScrollContainer/Malgxustvortlisto.get_children():
#		i.queue_free()
	for i in globala.gxust_vort_listo:
		var nova_vorto = baza_teksto.instance()
		nova_vorto.modulate = Color("20c0ff")
		nova_vorto.text = i
		$MarginContainer/Ujo/Gxustaj/ScrollContainer/Gxustvortlisto.add_child(nova_vorto)
		
	for i in globala.malgxust_vort_listo:
		var nova_vorto = baza_teksto.instance()
		nova_vorto.text = i
		nova_vorto.modulate = Color("ff2a00")
		$MarginContainer/Ujo/Gxustaj2/ScrollContainer/Malgxustvortlisto.add_child(nova_vorto)
		
	globala.forigi_vortliston()
		
		
#	kaj jen, LA butono surfekate de la muso
func _on_Kasxi_pressed():
	malaperi()
	
	
