extends Spatial

func _ready():
	print("butonoj kontrolitaj")

func _process(delta):
	if Input.is_action_just_pressed("left"):
		$text.text = "LEFT"
	if Input.is_action_just_pressed("right"):
		$text.text = "RIGHT"
	if Input.is_action_just_pressed("up"):
		$text.text = "UP"
	if Input.is_action_just_pressed("down"):
		$text.text = "DOWN"
		
	#"right up" kaj "left up" ne funkcias
	if Input.is_action_just_pressed("left_up"):
		$text.text = "LEFT_UP"
	if Input.is_action_just_pressed("left_down"):
		$text.text = "LEFT DOWN"
	if Input.is_action_just_pressed("right_up"):
		$text.text = "RIGHT UP"
	if Input.is_action_just_pressed("right_down"):
		$text.text = "RIGHT DOWN"
		
	#sistemaj butonoj, helpaj precipe por menuoj
	if Input.is_action_just_pressed("start"):
		$text.text = "START"
	if Input.is_action_just_pressed("select"):
		$text.text = "SELECT"
