extends ColorRect

var origine : Vector2

func _ready() -> void:
	visible = true
	origine.x = (OS.get_real_window_size().x / 2) - (rect_size.x / 2)
#	origine.x = rect_position.x

func _on_Emblemo_mouse_entered():
	$Tvino.interpolate_property(self, 'modulate', Color(1,1,1,0.5), Color(1,1,1,1.0), 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0) 
	$Tvino.start()


func _on_Emblemo_mouse_exited():
	$Tvino.interpolate_property(self, 'modulate', Color(1,1,1,1.0), Color(1,1,1,0.5), 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
func maldekstrigxi() -> void:
	$Tvino.interpolate_property(self, 'rect_position:x', rect_position.x, 0, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0) 
	$Tvino.start()
	
func centrigxi() -> void:
	$Tvino.interpolate_property(self, 'rect_position:x', rect_position.x, origine.x, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0) 
	$Tvino.start()
	

func _on_Emblemo_gui_input(event):
	if event is InputEventMouseButton:
		if !event.pressed:
			get_parent().peti_iri_al_pagxo("https://vortoj.net")
