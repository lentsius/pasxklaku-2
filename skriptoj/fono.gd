extends Node2D

export(Color, RGB) var frue
export(Color, RGB) var bone
export(Color, RGB) var mojose
export(Color, RGB) var nedivenite

var lingva_koloro
var origina_mem_modulado
var origina_modulado

# grafikefekta longeco en sekundoj
const efekta_longeco = 3

# en sekundoj
const help_vorta_longeco = 10


func _ready():
	if globala.sxargxi_agordon("agordoj", "muzik_lauteco", 100) == 0:
		$FlavaHelo.visible = false
		$FlavaHelo2.visible = false
		
	
	$DekstraBlanko.material.set_shader_param('koloro', globala.lingvo_koloro)
	$MaldekstraBlanko.material.set_shader_param('koloro', globala.lingvo_koloro)
	kasxi_koloron()
	origina_modulado = $etoso/partikloj.modulate
	origina_mem_modulado = $etoso/partikloj.self_modulate
	
	origina_modulado.a *= globala.efektokvanto
	
	print(origina_mem_modulado)
	lingva_koloro = globala.finbazkoloro
	if lingva_koloro == null:
		lingva_koloro = globala.lingvo_koloro
	help_vortoj()
	
	#Se griza estas TRUE, tiam ni ĝisdatigas nur la partiklojn kaj tiajn aferaĉojn.
	gxisdatigi_fonon(globala.griza)
		
	tradukigxi()
	
	if globala.lingvo_vorto == "ru":
		$etoso/efekta_artiklo.self_modulate = Color(1,1,1,0.1)
		$etoso/efekta_artiklo2.self_modulate = Color(1,1,1,0.1)
	else:
		$etoso/efekta_artiklo.self_modulate = Color("07ffffff")
		$etoso/efekta_artiklo2.self_modulate = Color("07ffffff")	
	
	gxisdatigi_limaltecojn()
	
	#sxangxas la fortecon de la partikloj lau ludantelekto
	$etoso/partikloj2.modulate.a *= globala.efektokvanto
	$etoso/partikloj.modulate.a *= globala.efektokvanto

	
func _process(delta) -> void:
	var lauteco = abs(AudioServer.get_bus_peak_volume_left_db(0,0))
	print(lauteco / 32)
	
	$FlavaHelo.modulate.a = (lauteco / 125) * globala.efektokvanto
	$FlavaHelo2.modulate.a = (lauteco / 125) * globala.efektokvanto

	
func gxisdatigi_limaltecojn():
	globala.frualteco = $etoso/alteclinio3.position.y
	globala.bonalteco = $etoso/alteclinio2.position.y
	globala.mojosalteco = $etoso/alteclinio.position.y

	
func gxisdatigi_fonon(duone : bool = false):
	if !duone:
		$verfono.modulate = lingva_koloro
	for i in $etoso.get_children():
		if !i.is_in_group("static_colour") and i.has_method("set_modulate"):
				i.modulate = lingva_koloro
			

func _on_ludo_frua_diveno(ludanto):
	partikla_efekto(ludanto, 1, frue)


func _on_ludo_bona_diveno(ludanto):
	partikla_efekto(ludanto, 2, bone)


func _on_ludo_mojosa_diveno(ludanto):
	partikla_efekto(ludanto, 3, mojose)


func _on_ludo_nedivenite(ludanto):
	partikla_efekto(ludanto, 1, nedivenite)


func _on_ludo_diveno(ludanto, tipo):
	match tipo:
		"frue":
			partikla_efekto(ludanto, 1, frue)
		"bone":
			partikla_efekto(ludanto, 2, bone)
		"mojose":
			partikla_efekto(ludanto, 3, mojose)

# forteco
# 1 - frue
# 2 - bone
# 3 - mojose


func partikla_efekto(ludanto, forteco, koloro = null):
	var nodo : Particles2D
	#warning-ignore:unused_variable
	var partiklo
	
	#la travidebleco estas agordita lau uzanta elekto
	forteco *= globala.efektokvanto
	
	if forteco < 0.1:
		return
	
	if ludanto == 1:
		nodo = $etoso/partikloj
		partiklo = nodo.get_process_material()
		$etoso/tvino.interpolate_property($maldekstra_linio, 'default_color', $maldekstra_linio.default_color, koloro, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 0)
		$Tvino.interpolate_property($MaldekstraBlanko, 'modulate', Color(1,1,1,forteco), Color(1,1,1,0), efekta_longeco, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	else:
		nodo = $etoso/partikloj2
		partiklo = nodo.get_process_material()
		$etoso/tvino.interpolate_property($dekstra_linio, 'default_color', $maldekstra_linio.default_color, koloro, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 0)			
		$Tvino.interpolate_property($DekstraBlanko, 'modulate', Color(1,1,1,forteco), Color(1,1,1,0), efekta_longeco, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
		
#	$etoso/tvino.interpolate_property(nodo, 'self_modulate', Color(1,1,1,forteco), origina_mem_modulado, efekta_longeco, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT, 0)
	if koloro != null:
		koloro.a = globala.efektokvanto
		$etoso/tvino.interpolate_property(nodo, 'modulate', koloro, lingva_koloro, efekta_longeco, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 0)			
#	$etoso/tvino.interpolate_property(partiklo, 'scale', forteco * 5, 5, efekta_longeco, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 0)
	$etoso/tvino.interpolate_property(nodo, 'lifetime', forteco * 5, 0.8, efekta_longeco, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 0)	
	$etoso/tvino.start()

	#Fonoj de la ludantoj
	$Tvino.start()
	
	
func kasxi_koloron() -> void:
	$Tvino.interpolate_property($MaldekstraBlanko, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 3, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($DekstraBlanko, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 3, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()

	
func tradukigxi():
	$etoso/Frue.text = TranslationServer.translate("FRUE")
	$etoso/Bone.text = TranslationServer.translate("BONE")
	$etoso/mojose.text = TranslationServer.translate("MOJOSE")

	
func help_vortoj():
	$etoso/tvino.interpolate_property($etoso/Frue, 'modulate', $etoso/Frue.modulate, Color(1,1,1,0), help_vorta_longeco, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$etoso/tvino.interpolate_property($etoso/Bone, 'modulate', $etoso/Frue.modulate, Color(1,1,1,0), help_vorta_longeco + 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$etoso/tvino.interpolate_property($etoso/mojose, 'modulate', $etoso/Frue.modulate, Color(1,1,1,0), help_vorta_longeco + 2, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$etoso/tvino.start()


