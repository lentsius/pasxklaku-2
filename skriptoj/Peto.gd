extends Control

var retpagxo : String = ""

var aktiva : = false

func _ready() -> void:
	$Ujo.modulate.a = 0
	modulate.a = 0
	visible = false


func eniri(destino : String) -> void:
	$Ujo/Teksto.text = "Iri al " + destino + "?"
	aktiva = true
	retpagxo = destino
	visible = true
	$Tvino.stop_all()
	$Tvino.interpolate_property(self, 'modulate:a', 0, 1, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($Ujo, 'modulate:a', 0, 1, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.5)
	$Tvino.start()
	
	
func eliri() -> void:
	aktiva = false
	retpagxo = ""
	$Tvino.stop_all()
	$Tvino.interpolate_property(self, 'modulate:a', 1, 0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($Ujo, 'modulate:a', 1, 0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.start()
	
	yield($Tvino, "tween_completed")
	
	visible = false
	get_parent().unua_butono.grab_focus()
	
func konfirmi_peton() -> void:
	OS.shell_open(retpagxo)
	eliri()
	
	
func nuligi_peton() -> void:
	eliri()
	
	
func _on_Jes_pressed():
	konfirmi_peton()


func _on_Ne_pressed():
	nuligi_peton()


func _on_Fono_gui_input(event):
	if event is InputEventMouseButton && aktiva:
		nuligi_peton()
