extends Control

onready var baza_kategorio : PackedScene = preload("res://assets/kategorioj/kategorio_label.tscn")
onready var menuujo : VBoxContainer = $koloro/VBoxContainer/ScrollContainer/menu
var kategorioj : Array

var gxustaj : Array = []
var malgxustaj : Array = []

var aktiva : = false

const minimumo_da_vortoj : int = 20

var avertante : bool = false

func _ready():
	self.visible = false
	$Averto.visible = false
	set_process(false)
	set_process_input(false)	

func aperi():
	globala.kategorio = "mikso"
	kontroli_vortojn()
	
	#la aliaj
	aktiva = true
	gxisdatigxi()
	$poenttabulo.gxisdatigxi()
	tradukigxi()
#	lauteco()	
	visible = true
	$animacio.play("eniro")
	yield($animacio, "animation_finished")
	set_process_input(true)	
	set_process(true)
	$koloro/VBoxContainer/ScrollContainer/menu.get_children()[0].grab_focus()

func malaperi():
	aktiva = false
		
	kategorioj.clear()
	
	for i in menuujo.get_children():
		i.queue_free()
		
	set_process(false)
	set_process_input(false)
	$animacio.play_backwards("eniro")
	yield($animacio, "animation_finished")
	self.visible = false
	get_parent().set_process(true)
	get_parent().unua_butono.grab_focus()	
	
# jen ni kontrolas kategoriojn
func gxisdatigxi():
	for i in menuujo.get_children():
		i.queue_free()
	
	kategorioj.clear()
	print("malplenegis kateogriojn, jen:", kategorioj)
	kategorioj = globala.preni_kategoriojn()
	
	
	for i in kategorioj:
		var nova = baza_kategorio.instance()
		nova.name = i
		nova.text = TranslationServer.translate(i)
		var teksto = nova.text
		nova.text = teksto.to_upper()
		menuujo.add_child(nova)
		nova.connect("pressed", self, "_kategorio_klakita")
	
	#metu mikson unuan
	menuujo.move_child(menuujo.get_node("mikso"), 0)
	var separator = HSeparator.new()
	separator.rect_min_size.x = 400.0
	separator.rect_min_size.y = 30.0
	separator.modulate.a = 0.2
	menuujo.add_child(separator)
	menuujo.move_child(separator, 1)
	menuujo.queue_sort()
	##konekti najbarojn
	var infanoj = menuujo.get_children()
	var aktiva : int = 0
	for i in infanoj:
		if aktiva + 1 < infanoj.size():
			if !i is Button:
				i.focus_neighbour_bottom = infanoj[aktiva + 1].get_path()
			aktiva += 1
			
	infanoj[0].focus_neighbour_top = infanoj[-1].get_path()
	infanoj[-1].focus_neighbour_bottom = infanoj[0].get_path()
	
	print(infanoj)
				
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if avertante:
			malaverti()
			return
			
		malaperi()
		
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_up"):
		for i in menuujo.get_children():
			if i.has_focus():
				globala.kategorio = i.name
						
				kontroli_vortojn()
				
	var glit_loko : float
	if menuujo.get_children().size() > 3:
		for i in menuujo.get_children():
			if i.has_focus():
				glit_loko = i.rect_position.y - i.rect_size.y * 2
		
		$koloro/VBoxContainer/ScrollContainer.scroll_vertical = lerp($koloro/VBoxContainer/ScrollContainer.scroll_vertical, glit_loko, 0.05)

func ludu_sonon(sono):
	if sono == "ek":
		$sonoj/ek.play()

func komencu():
	ludu_sonon("ek")
#	kaze de partikloj en la nodo. Provis ne sukcesis.
#	for partiklo in $partikloj.get_children():
#		partiklo.emitting = true
	yield($sonoj/ek, "finished")
	get_tree().change_scene('res://scenoj/gustamalgusta.tscn')

func _on_reen_pressed():
	malaperi()

func _kategorio_klakita():
	
	for i in menuujo.get_children():
		if i.has_focus():
			globala.kategorio = i.name
			
			if gxustaj.size() < 30 || malgxustaj.size() < 30:
				averti()
				return
			
			komencu()

#func _on_vestajxoj_pressed():
#	globala.kategorio = "vestajxoj"
#	komencu()

func tradukigxi():
	$koloro/VBoxContainer/kategorio.text = TranslationServer.translate("KATEGORIO")
	
func lauteco():
	if globala.lingvo_vorto != "eo":
		var nova_lauteco = AudioServer.get_bus_volume_db(2)
		AudioServer.set_bus_volume_db(2, nova_lauteco + 6)
	else:
		AudioServer.set_bus_volume_db(2, 0)
		
func kontroli_vortojn() -> void:
	gxustaj.clear()
	malgxustaj.clear()
	
	gxustaj = preni_gxustajn_vortojn()
	malgxustaj = preni_malgxustajn_vortojn()
	
		
func preni_gxustajn_vortojn() -> Array:
	var dosiero = File.new()
	dosiero.open(globala.lingva_dosierujo + "/" +  globala.lingvo_vorto + "/" + globala.kategorio + "/gxustaj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func preni_malgxustajn_vortojn() -> Array:
	var dosiero = File.new()
	dosiero.open(globala.lingva_dosierujo + "/" +  globala.lingvo_vorto + "/" + globala.kategorio + "/malgxustaj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj

#uzanto konsentis kaj scias ke estas malmulte da vortoj, tamen volas ludi
func _on_Bone_pressed() -> void:
	if avertante:
		malaverti()
		komencu()
	else:
		malaverti()


func _on_Reen_pressed() -> void:
	malaverti()


func averti() -> void:
	avertante = true
	$Averto.visible = true


func malaverti() -> void:
	$Averto.visible = false
	avertante = false
	$koloro/VBoxContainer/ScrollContainer/menu.get_child(0).grab_focus()
	
