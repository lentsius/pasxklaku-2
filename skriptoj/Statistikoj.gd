extends VBoxContainer

var nuna_l1_sukceso : float = 0
var nuna_l2_sukceso : float = 0

var fina_l1_sukceso : int = 0
var fina_l2_sukceso : int = 0

var lasta_l1 : int = 0
var lasta_l2 : int = 0

func _ready():
	set_process(false)
	#la gxeneralaj unue
	$Lingvo/LudLingvo.text = globala.lingvo_vorto
	$Malfacileco/LudMalfacileco.text = TranslationServer.translate(globala.malfacileco_vorto)
	
	var vortoj : int = globala.gxust_vort_listo.size() + globala.malgxust_vort_listo.size() + globala.volapukajxnombro
	
	#sukceselcentaĵoj
	var sukceso = ((globala.ludanto_1_statistikoj.frue / 3) + (globala.ludanto_1_statistikoj.bone / 1.5) + globala.ludanto_1_statistikoj.mojose) / vortoj
	var sukceso_l2 = ((globala.ludanto_2_statistikoj.frue / 3) + (globala.ludanto_2_statistikoj.bone / 1.5) + globala.ludanto_2_statistikoj.mojose) / vortoj
	#ni tvinu ilin
#	print("sukcesaj elcentaĵoj: ",sukceso, sukceso_l2)
	tvini_nombrojn(sukceso, sukceso_l2)
	
	
	#enmetu la statistikojn por ludanto unu
	
	$L1/Neklakite/Nombro.text = str(globala.ludanto_1_statistikoj.neklakite)
	$L1/Eraroj/Nombro.text = str(globala.ludanto_1_statistikoj.eraroj)
	$L1/Frue/Nombro.text = str(globala.ludanto_1_statistikoj.frue)
	$L1/Bone/Nombro.text = str(globala.ludanto_1_statistikoj.bone)
	$L1/Mojose/Nombro.text = str(globala.ludanto_1_statistikoj.mojose)
	
	#enmetu la statistikojn por ludanto du
	
	$L2/Neklakite/Nombro.text = str(globala.ludanto_2_statistikoj.neklakite)
	$L2/Eraroj/Nombro.text = str(globala.ludanto_2_statistikoj.eraroj)
	$L2/Frue/Nombro.text = str(globala.ludanto_2_statistikoj.frue)
	$L2/Bone/Nombro.text = str(globala.ludanto_2_statistikoj.bone)
	$L2/Mojose/Nombro.text = str(globala.ludanto_2_statistikoj.mojose)
	
	#kiam la statistikoj de ambaŭ ludantoj pretas ni restartu la statistik sistemon
	globala.restarti_statistikojn()
	

func tvini_nombrojn(l1 : float, l2 : float) -> void:
	fina_l1_sukceso = ceil(l1 * 100)
	fina_l2_sukceso = ceil(l2 * 100)
	
#	print("ni tvinas nunan sukcesojn al: ", fina_l1_sukceso,"% ", fina_l2_sukceso, "% ")
	$Tvino.interpolate_property(self, "nuna_l1_sukceso", 0, fina_l1_sukceso, 5.0, Tween.TRANS_CUBIC, Tween.EASE_IN, 0.0)
	$Tvino.interpolate_property(self, "nuna_l2_sukceso", 0, fina_l2_sukceso, 5.0, Tween.TRANS_CUBIC, Tween.EASE_IN, 0.0)

func komenci_tvinon() -> void:
	$Tvino.start()


func _on_Tvino_tween_step(object: Object, key: NodePath, elapsed: float, value: Object) -> void:
	if int(lasta_l2) != int(nuna_l2_sukceso):
		$L2/Sukceso/Fono/Centro/Nombro.text = str(ceil(nuna_l2_sukceso)) + "%"
		$L2/Sukceso/Fono.rect_min_size.x += 2
		lasta_l2 = nuna_l2_sukceso
		$KlaksonoL2.play()
	
	if int(lasta_l1) != int(nuna_l1_sukceso):
		$L1/Sukceso/Fono/Centro/Nombro.text = str(ceil(nuna_l1_sukceso)) + "%"
		$L1/Sukceso/Fono.rect_min_size.x += 2
		#ni ĝisdatigu tiujn ĉi por ke ni komprenu ke okazis ŝanĝo
		lasta_l1 = nuna_l1_sukceso
		$Klaksono.play()
