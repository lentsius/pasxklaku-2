extends VBoxContainer

var evento
var mapo
var sercxas = false
onready var mesagxo = get_parent().get_node("mesagxo")
var ludanto = 1
var unue = true


const INPUT_ACTIONS = [ "up", "down", "left", "right", "left_up", "left_down", "right_up", "right_down", "select", "start", "up_l2", "down_l2",
"left_l2", "right_l2", "left_up_l2", "left_down_l2", "right_up_l2", "right_down_l2", "select_l2", "start_l2"]

const agord_dosiero = "user://input.cfg"
const origina_mapado = "res://mapado/origina_mapado.cfg"

signal UI_agordita

func _ready():
	var opcio = $HBoxContainer2/LudantOpcio
	opcio.clear()
	opcio.add_item("1")
	opcio.add_item("2")	
	unue = globala.unue
	#ne skanu klakojn gxis kiam oni ne pasxklakas
	set_process_input(false)
	komencigxi()

func komencigxi():
	var config = ConfigFile.new()
	var err = config.load(agord_dosiero)
	if err: 
		err = config.load(origina_mapado)
		if err == OK:
#			#sxargis la originan mapadon
			config.save(agord_dosiero)
		else:
			print("Averto! Malsukcese sxargis la originan mapadon, vi devas mapi gxin mem!")
#			break
#		# Tiu cxi ne uzu se vi jam havas la originan mapadaon
#		# Assuming that file is missing, generate default config
#		for action_name in INPUT_ACTIONS:
#			var action_list = InputMap.get_action_list(action_name)
#
#			# There could be multiple actions in the list, but we save the first one by default
#			var button_index = action_list[0].button_index
#			var enigilo_indekso = action_list[0].device
#			config.set_value("input", action_name, button_index)
#			config.set_value("input_enigilo", action_name, enigilo_indekso)			

#	else: 
	# ConfigFile was properly loaded, initialize InputMap
	for action_name in config.get_section_keys("input"):
		
		# Get the key scancode corresponding to the saved human-readable string
		var button_index = config.get_value("input", action_name)
		var enigilo = config.get_value("input_enigilo", action_name)
		
		# Create a new event object based on the saved scancode
		var event = InputEventJoypadButton.new()
		event.button_index = button_index
		if enigilo != null:
			event.device = enigilo
		else:
			event.device = 0
		# Replace old action (key) events by the new one
		for old_event in InputMap.get_action_list(action_name):
			if old_event is InputEventJoypadButton:
				InputMap.action_erase_event(action_name, old_event)
		InputMap.action_add_event(action_name, event)
	
	# certigi ke la klakoj de la ludo estas en ordo
	yield(get_tree().create_timer(1.0), "timeout")
	if unue:
		revivigi_interfacajn_klakojn()
	yield(get_tree().create_timer(1.0), "timeout")
	revivigi_ludant_klavojn()	

func sxangxi_butonon(sercxmapo):
	if ludanto == 2:
		sercxmapo += "_l2"
	mesagxo.visible = true
	sercxas = true
	mapo = sercxmapo
	set_process_input(true)
	

func sxangxite():
	print("Sukcese sxangxis la mapon de " + mapo + " per ")
	print(evento)
	mesagxo.visible = false
	sercxas = false
	mapo = ""

func nuligite():
	print("neniu mapo sxangxita")
	mesagxo.visible = false
	sercxas = false
	mapo = ""
	
func origine_mapi() -> void:
	var origina = ConfigFile.new()
	var eraro = origina.load(origina_mapado)
	if eraro == OK:
		origina.save(agord_dosiero)
		komencigxi()
	else:
		print("ne trovis la originan mapadon, bnvl reinstali la ludon.")

func konservi(sekcio, butono, kodo, enigilo):
	"""Helper function to redefine a parameter in the settings file"""
	var config = ConfigFile.new()
	var err = config.load(agord_dosiero)
	if err:
		print("Error code when loading config file: ", err)
	else:
		config.set_value(sekcio, butono, kodo)
		config.set_value(sekcio + "_enigilo", butono, enigilo)		
		config.save(agord_dosiero)	
		
func konservi_bazan():
	### helpfunkcio kiu kopias la agordojn al 
	var dir = Directory.new()
	var err = dir.copy("user://input.cfg", "user://input_backup.cfg")
	print(err, "provis konervi la dosieron")

func refresxigi_bazan():
	### helpfunkcio kiu kopias la agordojn al 
	var dir = Directory.new()
	dir.copy("user://input_backup.cfg", "user://input.cfg")
	komencigxi()

func _on_select_pressed():
	sxangxi_butonon("select")

func _on_start_pressed():
	sxangxi_butonon("start")

func _on_supren_pressed():
	sxangxi_butonon("up")

func _on_maldesktren_pressed():
	sxangxi_butonon("left")

func _on_dekstren_pressed():
	sxangxi_butonon("right")

func _on_suben_pressed():
	sxangxi_butonon("down")

func _on_suprdekstren_pressed():
	sxangxi_butonon("right_up")

func _on_submaldekstren_pressed():
	sxangxi_butonon("left_down")

func _on_suprmaldesktren_pressed():
	sxangxi_butonon("left_up")

func _on_subdekstren_pressed():
	sxangxi_butonon("right_down")

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel") && sercxas:
		nuligite()
#	if Input.is_action_just_pressed("cheat"):
#		konservi_bazan()
#	if Input.is_action_just_pressed("sxargi_agordojn"):
#		refresxigi_bazan()


func _on_origine_mapi_pressed():
	origine_mapi()
	
	
func _input(event):
	if event is InputEventJoypadButton:
		
		if !sercxas:
			return
		if !InputMap.has_action(mapo):
			print("MAPO NE TROVITA")
			nuligite()
			return
			
		# Register the event as handled and stop polling
		get_tree().set_input_as_handled()
		set_process_input(false)			
			
		InputMap.action_erase_events(mapo)
		InputMap.action_add_event(mapo, event)
		
		if mapo == "up":
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Up")
			InputMap.action_erase_events("ui_up")
			InputMap.action_add_event("ui_up", klavo)			
			InputMap.action_add_event("ui_up", InputMap.get_action_list("up")[0])
			InputMap.action_add_event("ui_up", InputMap.get_action_list("up_l2")[0])
			
		elif mapo == "left":
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Left")
			InputMap.action_erase_events("ui_left")
			InputMap.action_add_event("ui_left", klavo)			
			InputMap.action_add_event("ui_left", InputMap.get_action_list("left")[0])
			InputMap.action_add_event("ui_left", InputMap.get_action_list("left_l2")[0])

		elif mapo == "right":
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Right")
			InputMap.action_erase_events("ui_right")
			InputMap.action_add_event("ui_right", klavo)
			InputMap.action_add_event("ui_right", InputMap.get_action_list("right")[0])
			InputMap.action_add_event("ui_right", InputMap.get_action_list("right_l2")[0])
			
		elif mapo == "down":
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Down")
			InputMap.action_erase_events("ui_down")
			InputMap.action_add_event("ui_down", klavo)
			InputMap.action_add_event("ui_down", InputMap.get_action_list("down")[0])
			InputMap.action_add_event("ui_down", InputMap.get_action_list("down_l2")[0])
		
		elif mapo == "select":
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Space")
			InputMap.action_erase_events("ui_select")
			InputMap.action_add_event("ui_select", klavo)
			InputMap.action_add_event("ui_select", InputMap.get_action_list("select")[0])
			InputMap.action_add_event("ui_select", InputMap.get_action_list("select_l2")[0])
			
		elif mapo == "start":
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Enter")
			InputMap.action_erase_events("ui_accept")
			InputMap.action_add_event("ui_accept", klavo)
			InputMap.action_add_event("ui_accept", InputMap.get_action_list("start")[0])
			InputMap.action_add_event("ui_accept", InputMap.get_action_list("start_l2")[0])
		
		var kodo = event.button_index
		var enigilo = event.device		
		
		evento = event
		konservi("input", mapo, kodo, enigilo)
		sxangxite()


func _on_nuligi_pressed():
	nuligite()
	
func revivigi_interfacajn_klakojn():
	for i in range(0,9):
#		print(i)
		if i == 1:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Up")
			var klavo_du = InputEventKey.new()
			klavo_du.scancode = OS.find_scancode_from_string("W")
			InputMap.action_erase_events("ui_up")
			InputMap.action_add_event("ui_up", klavo)			
			InputMap.action_add_event("ui_up", klavo_du)			
			InputMap.action_add_event("ui_up", InputMap.get_action_list("up")[0])
			InputMap.action_add_event("ui_up", InputMap.get_action_list("up_l2")[0])
			
		elif i == 2:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Left")
			var klavo_du = InputEventKey.new()
			klavo_du.scancode = OS.find_scancode_from_string("A")			
			InputMap.action_erase_events("ui_left")
			InputMap.action_add_event("ui_left", klavo)			
			InputMap.action_add_event("ui_left", klavo_du)			
			InputMap.action_add_event("ui_left", InputMap.get_action_list("left")[0])
			InputMap.action_add_event("ui_left", InputMap.get_action_list("left_l2")[0])

		elif i == 3:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Right")
			var klavo_du = InputEventKey.new()
			klavo_du.scancode = OS.find_scancode_from_string("D")			
			InputMap.action_erase_events("ui_right")
			InputMap.action_add_event("ui_right", klavo)
			InputMap.action_add_event("ui_right", klavo_du)
			InputMap.action_add_event("ui_right", InputMap.get_action_list("right")[0])
			InputMap.action_add_event("ui_right", InputMap.get_action_list("right_l2")[0])
			
		elif i == 4:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Down")
			var klavo_du = InputEventKey.new()
			klavo_du.scancode = OS.find_scancode_from_string("S")			
			InputMap.action_erase_events("ui_down")
			InputMap.action_add_event("ui_down", klavo)
			InputMap.action_add_event("ui_down", klavo_du)
			InputMap.action_add_event("ui_down", InputMap.get_action_list("down")[0])
			InputMap.action_add_event("ui_down", InputMap.get_action_list("down_l2")[0])
		
		elif i == 5:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Escape")
			InputMap.action_erase_events("ui_cancel")
			InputMap.action_add_event("ui_cancel", klavo)
			InputMap.action_add_event("ui_cancel", InputMap.get_action_list("select")[0])
			InputMap.action_add_event("ui_cancel", InputMap.get_action_list("select_l2")[0])

		elif i == 6:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Space")
			InputMap.action_erase_events("ui_select")
			InputMap.action_add_event("ui_select", klavo)
			InputMap.action_add_event("ui_select", InputMap.get_action_list("select")[0])
			InputMap.action_add_event("ui_select", InputMap.get_action_list("select_l2")[0])
			
		elif i == 7:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Enter")
			InputMap.action_erase_events("ui_accept")
			InputMap.action_add_event("ui_accept", klavo)
			InputMap.action_add_event("ui_accept", InputMap.get_action_list("start")[0])
			InputMap.action_add_event("ui_accept", InputMap.get_action_list("start_l2")[0])
			
	unue = false
	globala.unue = false
			

func revivigi_ludant_klavojn():
	for i in range(0,9):
#		print(i)
		#w
		if i == 1:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("W")
			InputMap.action_add_event("up", klavo)			

		#a	
		if i == 2:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("A")
			InputMap.action_add_event("left", klavo)			

		#s
		if i == 3:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("S")
			InputMap.action_add_event("down", klavo)			

		#d	
		if i == 4:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("D")
			InputMap.action_add_event("right", klavo)			

		#up
		if i == 5:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Up")
			InputMap.action_add_event("up_l2", klavo)			

		#left
		if i == 6:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Left")
			InputMap.action_add_event("left_l2", klavo)			

		#right
		if i == 7:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Right")
			InputMap.action_add_event("right_l2", klavo)

		#down	
		if i == 8:
			var klavo = InputEventKey.new()
			klavo.scancode = OS.find_scancode_from_string("Down")
			InputMap.action_add_event("down_l2", klavo)

func _on_ludanto_1_toggled(button_pressed):
	if button_pressed:
		$HBoxContainer2/ludanto_2.pressed = 0
		ludanto = 1
		get_parent().get_node("buton_montrilo").ludanto = 1
		
	else:
		$HBoxContainer2/ludanto_2.pressed = 1
		ludanto = 2
		get_parent().get_node("buton_montrilo").ludanto = 2		

func _on_ludanto_2_toggled(button_pressed):
	if button_pressed:
		$HBoxContainer2/ludanto_1.pressed = 0
		ludanto = 2
		get_parent().get_node("buton_montrilo").ludanto = 2		
		
	else:
		$HBoxContainer2/ludanto_1.pressed = 1
		ludanto = 1
		get_parent().get_node("buton_montrilo").ludanto = 1		


func _on_LudantOpcio_item_selected(ID):
	ludanto = ID + 1
	print(ludanto)
