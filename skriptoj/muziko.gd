extends Node

var nunakanto = null

func _ready():
	ludu_hazardan()
	
func _process(delta):
	if Input.is_action_just_pressed("venontkanta"):
		ludu_hazardan()

func ludu_hazardan():
#	randomize()
	var kantoj = get_children()
	var hazard = randi() % kantoj.size()
	
	for i in kantoj:
		i.stop()
	
	kantoj[hazard].play()
	nunakanto = kantoj[hazard]

func _on_m1_finished():
	ludu_hazardan()


func _on_m2_finished():
	ludu_hazardan()


func _on_m3_finished():
	ludu_hazardan()
