extends KinematicBody2D

var tipo = 1
var teksto = ""

var l1_klakis = false
var l2_klakis = false

var rapideco

export(Color, RGBA) var rugxa = Color(1,0,0)
export(Color, RGBA) var orangxa = Color(1,0,0)
export(Color, RGBA) var blua = Color(0,0,1)
export(Color, RGBA) var flava = Color(0.5,0.5,0)

func _ready():
	$teksto.text = teksto
	get_parent().vortoj_en_la_ludo += 1

func _process(delta):
	var movi = Vector2(0, rapideco * delta * globala.rapideco)
	#videbligxi()
	translate(movi)
	
	
func vortigi(tio):
	teksto = tio
	$teksto.text = tio
	
	
func modifi_rapidecon(kiom):
	rapideco = kiom
	
func klakate(ludanto, elektita_tipo):
	match ludanto:
		1:
			l1_klakis = true
		2:
			l2_klakis = true
			
	var diveno : bool = false 
	var modo : String = "frue"
	var sinsekvo : bool = false
	
	modo = kontroli_modon()
	
	if elektita_tipo == tipo:
		diveno = true
		globala.divenite(ludanto,modo)
		aldonajxoj(ludanto, modo)
		if modo == "mojose":
			sinsekvo = true
		
	else:
		aldonajxoj(ludanto, "nedivenite")
		globala.mistrafis(ludanto)
	
	# Sendi informojn al la globala por ke ni povu poste trakti kiom bone oni ludis
	if ludanto == 1 && diveno:
		globala.ludanto_1_divenitaj.append($teksto.text)
	elif ludanto == 1 && !diveno:
		globala.ludanto_1_nedivenitaj.append($teksto.text)
	
	
	return [diveno, modo, sinsekvo]
	
func kontroli_modon() -> String:
	var pos : float = position.y - 655.929
	var modo : String
	
	if pos > globala.mojosalteco + 25:
		modo = "bone"
	elif pos > globala.mojosalteco:
		modo = "mojose"
	elif pos > globala.bonalteco:
		modo = "bone"
	else:
		modo = "frue"
		
#	print("Vorta pozicio: ", pos, "Pozicio de la mojoslinio: ", globala.mojosalteco)
	return modo
	
	
func maltrafi():
	get_parent().vortoj_en_la_ludo -= 1
	get_parent().kontroli_vortojn()
	if !l1_klakis:
		globala.ludanto_1_statistikoj.neklakite += 1
		globala.ludo.l1_kombo = 0
		globala.gui.gxisdatigi_kombon(1, 0)
	if !l2_klakis:
		globala.ludanto_2_statistikoj.neklakite += 1
		globala.ludo.l2_kombo = 0
		globala.gui.gxisdatigi_kombon(2, 0)
		
	match tipo:
		1:
			globala.gxust_vort_listo.append($teksto.text)
		2:
			globala.malgxust_vort_listo.append($teksto.text)
		3:
			globala.volapukajxnombro += 1
			
	self.queue_free()
	
	
#0 -> 300 
func videbligxi():
	var travidebleco = position.y / 980
	modulate = Color(1,1,1,travidebleco)
	
func aldonajxoj(ludanto, modo):
	var koloro
	if modo == "frue":
		koloro = orangxa
	elif modo == "bone":
		koloro = blua
	elif modo == "nedivenite":
		koloro = rugxa
	else:
		koloro = flava
		
	if ludanto == 1:
		l1_klakis = true
		$l.visible = true
		$l.modulate = koloro
		var videbla_koloro = koloro
		videbla_koloro.a = 1
		$l_tvino.interpolate_property($l, "modulate", koloro, videbla_koloro, 0.2, Tween.TRANS_BOUNCE, Tween.EASE_IN, 0) 
		$l_tvino.start()
	else:
		l2_klakis = true
		$r.visible = true
		$r.modulate = koloro
		var videbla_koloro = koloro
		videbla_koloro.a = 1
		$r_tvino.interpolate_property($r, "modulate", koloro, videbla_koloro, 0.2, Tween.TRANS_BOUNCE, Tween.EASE_IN, 0) 
		$r_tvino.start()
	
func fino():
	$tvino.interpolate_property(self, "modulate", self.get_modulate(), Color(1,1,1,0), 0.95, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	$tvino.start()
	
